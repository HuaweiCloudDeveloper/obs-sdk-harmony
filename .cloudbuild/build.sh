#huaweicloud-obs-sdk-browserjs_{ver}.zip
export version=$SDKVersion

if [[ $CloudType == "" ]]; then
    CloudType="huaweicloud"
fi
CloudType=${CloudType,,}

export zipPackageName=${CloudType}-obs-sdk-harmony
zip -r ${zipPackageName}.zip ./ -x ".cloudbuild/*" -x ".git/*" -x ".gitignore"
IS_RELEASE=`echo "$IS_RELEASE" | tr 'a-z' 'A-Z'`
if [ "$IS_RELEASE"x == "FALSE"x ]; then
    zip -q -r ${zipPackageName}_$SDKVersion-$CID_BUILD_TIME.zip ${zipPackageName}.zip
else
    zip -q -r ${zipPackageName}_$SDKVersion.zip  ${zipPackageName}.zip
fi