/**
 * Copyright 2019 Huawei Technologies Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 */

import {
    objectEncryptionRule,
    commonPrefixes,
    owner,
    initiator
} from "./baseModel"

export const UploadPart = {
    'httpMethod': 'PUT',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Key': {
            'required': true,
            'location': 'uri',
        },
        'PartNumber': {
            'required': true,
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'partNumber',
        },
        'UploadId': {
            'required': true,
            'location': 'urlPath',
            'sentAs': 'uploadId',
        },
        'ContentMD5': {
            'location': 'header',
            'sentAs': 'Content-MD5',
        },
        'ContentSHA256': {
            'location': 'header',
            'sentAs': 'content-sha256',
            'withPrefix': true,
        },
        'Body': {
            'location': 'body',
        },
        'SourceFile': {
            'type': 'srcFile',
        },
        'Offset': {
            'type': 'plain'
        },
        'PartSize': {
            'type': 'number',
        },
        ...objectEncryptionRule,
        'ProgressCallback': {
            'type': 'plain'
        },
    },
}

export const UploadPartOutput = {
    'parameters': {
        'ETag': {
            'location': 'header',
            'sentAs': 'etag',
        },
        ...objectEncryptionRule,
    },
}

export const CompleteMultipartUpload = {
    'httpMethod': 'POST',
    'data': {
        'xmlRoot': 'CompleteMultipartUpload',
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Key': {
            'required': true,
            'location': 'uri',
        },
        'UploadId': {
            'required': true,
            'location': 'urlPath',
            'sentAs': 'uploadId',
        },
        'EncodingType': {
            'location': 'urlPath',
            'sentAs': 'encoding-type',
        },
        ...objectEncryptionRule,
        'Parts': {
            'required': true,
            'type': 'array',
            'location': 'xml',
            'sentAs': 'Part',
            'items': {
                'type': 'object',
                'parameters': {
                    'PartNumber': {
                        'sentAs': 'PartNumber',
                        'type': 'number',
                    },
                    'ETag': {
                        'sentAs': 'ETag',
                    },
                },
            },
        },
        'Callback': {
            'location': 'header',
            'sentAs': 'callback',
            'withPrefix': true,
            'type': 'callback',
            'parameters': {
                'CallbackUrl': {
                    'required': true,
                },
                'CallbackBody': {
                    'required': true,
                },
                'CallbackHost': {},
                'CallbackBodyType': {},
            }
        },
    },
}

export const CompleteMultipartUploadOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'CompleteMultipartUploadResult',
    },
    'parameters': {
        'VersionId': {
            'location': 'header',
            'sentAs': 'version-id',
            'withPrefix': true
        },
        'Location': {
            'location': 'xml',
            'sentAs': 'Location',
        },
        'EncodingType': {
            'location': 'xml',
            'sentAs': 'EncodingType',
        },
        'Bucket': {
            'location': 'xml',
            'sentAs': 'Bucket',
        },
        'Key': {
            'decode': true,
            'location': 'xml',
            'sentAs': 'Key',
        },
        'ETag': {
            'location': 'xml',
            'sentAs': 'ETag',
        },
        ...objectEncryptionRule,
    },
    "CallbackResponse": {
        'location': 'body',
        'sentAs': 'CallbackResponseBody'
    }
}

export const ListParts = {
    'httpMethod': 'GET',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Key': {
            'required': true,
            'location': 'uri',
        },
        'UploadId': {
            'required': true,
            'location': 'urlPath',
            'sentAs': 'uploadId',
        },
        'MaxParts': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'max-parts',
        },
        'PartNumberMarker': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'part-number-marker',
        },
        'EncodingType': {
            'location': 'urlPath',
            'sentAs': 'encoding-type',
        },
    },
}

export const ListPartsOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'ListPartsResult',
    },
    'parameters': {
        'Bucket': {
            'location': 'xml',
            'sentAs': 'Bucket',
        },
        'Key': {
            'decode': true,
            'location': 'xml',
            'sentAs': 'Key',
        },
        'UploadId': {
            'location': 'xml',
            'sentAs': 'UploadId',
        },
        'PartNumberMarker': {
            'location': 'xml',
            'sentAs': 'PartNumberMarker',
        },
        'NextPartNumberMarker': {
            'location': 'xml',
            'sentAs': 'NextPartNumberMarker',
            'type': 'number',
        },
        'MaxParts': {
            'location': 'xml',
            'sentAs': 'MaxParts',
            'type': 'number',
        },
        'IsTruncated': {
            'location': 'xml',
            'sentAs': 'IsTruncated',
            'type': 'boolean',
        },
        'StorageClass': {
            'location': 'xml',
            'sentAs': 'StorageClass',
        },
        'EncodingType': {
            'location': 'urlPath',
            'sentAs': 'EncodingType',
        },
        'Initiator': initiator,
        'Owner': owner,
        'Parts': {
            'type': 'array',
            'location': 'xml',
            'sentAs': 'Part',
            'items': {
                'type': 'object',
                'parameters': {
                    'PartNumber': {
                        'sentAs': 'PartNumber',
                        'type': 'number',
                    },
                    'LastModified': {
                        'sentAs': 'LastModified',
                    },
                    'ETag': {
                        'sentAs': 'ETag',
                    },
                    'Size': {
                        'sentAs': 'Size',
                        'type': 'number',
                    },
                },
            },
        }
    },
}

export const CopyPart = {
    'httpMethod': 'PUT',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Key': {
            'required': true,
            'location': 'uri',
        },
        'PartNumber': {
            'required': true,
            'location': 'urlPath',
            'sentAs': 'partNumber',
            'type': 'number',
        },
        'UploadId': {
            'required': true,
            'location': 'urlPath',
            'sentAs': 'uploadId',
        },
        'CopySource': {
            'required': true,
            'location': 'header',
            'sentAs': 'copy-source',
            'skipEncoding': true,
            'withPrefix': true
        },
        'CopySourceRange': {
            'location': 'header',
            'sentAs': 'copy-source-range',
            'withPrefix': true
        },
        'SseC': {
            'location': 'header',
            'sentAs': 'server-side-encryption-customer-algorithm',
            'withPrefix': true
        },
        'SseCKey': {
            'location': 'header',
            'sentAs': 'server-side-encryption-customer-key',
            'type': 'password',
            'withPrefix': true
        },
        'CopySourceSseC': {
            'location': 'header',
            'sentAs': 'copy-source-server-side-encryption-customer-algorithm',
            'withPrefix': true
        },
        'CopySourceSseCKey': {
            'location': 'header',
            'sentAs': 'copy-source-server-side-encryption-customer-key',
            'type': 'password',
            'withPrefix': true
        }
    },
}

export const CopyPartOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'CopyPartResult',
    },
    'parameters': {
        'LastModified': {
            'location': 'xml',
            'sentAs': 'LastModified',
        },
        'ETag': {
            'location': 'xml',
            'sentAs': 'ETag',
        },
        ...objectEncryptionRule,
    },
}

export const AbortMultipartUpload = {
    'httpMethod': 'DELETE',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Key': {
            'required': true,
            'location': 'uri',
        },
        'UploadId': {
            'required': true,
            'location': 'urlPath',
            'sentAs': 'uploadId',
        },
    },
}

export const ListMultipartUploads = {
    'httpMethod': 'GET',
    'urlPath': 'uploads',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Delimiter': {
            'location': 'urlPath',
            'sentAs': 'delimiter',
        },
        'KeyMarker': {
            'location': 'urlPath',
            'sentAs': 'key-marker',
        },
        'MaxUploads': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'max-uploads',
        },
        'Prefix': {
            'location': 'urlPath',
            'sentAs': 'prefix',
        },
        'UploadIdMarker': {
            'location': 'urlPath',
            'sentAs': 'upload-id-marker',
        },
        'EncodingType': {
            'location': 'urlPath',
            'sentAs': 'encoding-type',
        }
    },
}

export const ListMultipartUploadsOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'ListMultipartUploadsResult',
    },
    'parameters': {
        'Bucket': {
            'location': 'xml',
            'sentAs': 'Bucket',
        },
        'KeyMarker': {
            'decode': true,
            'location': 'xml',
            'sentAs': 'KeyMarker',
        },
        'UploadIdMarker': {
            'location': 'xml',
            'sentAs': 'UploadIdMarker',
        },
        'NextKeyMarker': {
            'decode': true,
            'location': 'xml',
            'sentAs': 'NextKeyMarker',
        },
        'Prefix': {
            'decode': true,
            'location': 'xml',
            'sentAs': 'Prefix',
        },
        'Delimiter': {
            'decode': true,
            'location': 'xml',
            'sentAs': 'Delimiter',
        },
        'NextUploadIdMarker': {
            'location': 'xml',
            'sentAs': 'NextUploadIdMarker',
        },
        'MaxUploads': {
            'location': 'xml',
            'sentAs': 'MaxUploads',
            'type': 'number',
        },
        'IsTruncated': {
            'location': 'xml',
            'sentAs': 'IsTruncated',
            'type': 'boolean',
        },
        'EncodingType': {
            'location': 'xml',
            'sentAs': 'EncodingType'
        },
        'Uploads': {
            'type': 'array',
            'location': 'xml',
            'sentAs': 'Upload',
            'items': {
                'type': 'object',
                'parameters': {
                    'UploadId': {
                        'sentAs': 'UploadId',
                    },
                    'Key': {
                        'decode': true,
                        'sentAs': 'Key',
                    },
                    'Initiated': {
                        'sentAs': 'Initiated',
                    },
                    'StorageClass': {
                        'sentAs': 'StorageClass',
                    },
                    'Owner': owner,
                    'Initiator': initiator
                },
            },
        },
        'CommonPrefixes': commonPrefixes
    },
}

export const InitiateMultipartUpload = {
    'httpMethod': 'POST',
    'urlPath': 'uploads',
    'parameters': {
        'EncodingType': {
            'location': 'urlPath',
            'sentAs': 'encoding-type',
        },
        'ObjectLockMode': {
            'location': 'header',
            'sentAs': 'object-lock-mode',
            'withPrefix': true
        },
        'ObjectLockRetainUntailDate': {
            'location': 'header',
            'sentAs': 'object-lock-retain-until-date',
            'withPrefix': true
        },
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Key': {
            'required': true,
            'location': 'uri',
        },
        'Acl': {
            'location': 'header',
            'sentAs': 'acl',
            'withPrefix': true,
            'type': 'adapter'
        },
        'GrantRead': {
            'location': 'header',
            'sentAs': 'grant-read',
            'withPrefix': true,
        },
        'GrantReadAcp': {
            'location': 'header',
            'sentAs': 'grant-read-acp',
            'withPrefix': true,
        },
        'GrantWriteAcp': {
            'location': 'header',
            'sentAs': 'grant-write-acp',
            'withPrefix': true,
        },
        'GrantFullControl': {
            'location': 'header',
            'sentAs': 'grant-full-control',
            'withPrefix': true,
        },
        'StorageClass': {
            'location': 'header',
            'sentAs': 'storage-class',
            'withPrefix': true,
            'type': 'adapter'
        },
        'Metadata': {
            'type': 'object',
            'location': 'header',
            'sentAs': 'meta-',
            'withPrefix': true,
        },
        'WebsiteRedirectLocation': {
            'location': 'header',
            'sentAs': 'website-redirect-location',
            'withPrefix': true,
        },
        'Tags': {
            'location': 'header',
            'sentAs': 'tagging',
            'withPrefix': true,
        },
        'Expires': {
            'location': 'header',
            'sentAs': 'x-obs-expires',
            'type': 'number'
        },
        'ContentType': {
            'location': 'header',
            'sentAs': 'Content-Type'
        },
        'CacheControl': {
            'location': 'header',
            'sentAs': 'Cache-Control'
        },
        'ContentDisposition': {
            'location': 'header',
            'sentAs': 'Content-Disposition',
            'encodingSafe': ' ;/?:@&=+$,"'
        },
        'ContentLanguage': {
            'location': 'header',
            'sentAs': 'Content-Language'
        },
        'ContentEncoding': {
            'location': 'header',
            'sentAs': 'Content-Encoding'
        },
        ...objectEncryptionRule,
    },
}

export const InitiateMultipartUploadOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'InitiateMultipartUploadResult',
    },
    'parameters': {
        'EncodingType': {
            'location': 'xml',
            'sentAs': 'EncodingType',
        },
        'Bucket': {
            'location': 'xml',
            'sentAs': 'Bucket',
        },
        'Key': {
            'decode': true,
            'location': 'xml',
            'sentAs': 'Key',
        },
        'UploadId': {
            'location': 'xml',
            'sentAs': 'UploadId',
        },
        ...objectEncryptionRule,
    },
}
