/**
 * Copyright 2019 Huawei Technologies Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 */

import {
    backToSourceRules,
    CDNNotifyConfiguration,
    objectLock,
    templates
} from "./baseModel";

export const GetBucketCDNNotifyConfiguration = {
    'httpMethod': 'GET',
    'urlPath': 'CDNNotifyConfiguration',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'NotForwardTag': {
            'type': 'string',
            'sentAs': 'x-obs-not-forward-tag',
            'location': 'header'
        },
    }
}

export const GetBucketCDNNotifyConfigurationOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'CDNNotifyConfiguration'
    },
    'parameters': {
        'Domain': CDNNotifyConfiguration
    }
}

export const SetBucketCdnNotifyConfiguration = {
    'httpMethod': 'PUT',
    'urlPath': 'CDNNotifyConfiguration',
    'data': {
        'xmlRoot': 'CDNNotifyConfiguration',
        'xmlAllowEmpty': true
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'NotForwardTag': {
            'type': 'string',
            'sentAs': 'x-obs-not-forward-tag',
            'location': 'header'
        },
        'Domain': CDNNotifyConfiguration
    },
}


export const SetBackToSource = {
    'httpMethod': 'PUT',
    'data': {
        'xmlRoot': 'BackToSourceConfiguration',
    },
    'urlPath': 'backtosource',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'BackToSourceRules': backToSourceRules,
        'ContentMD5': {
            'location': 'header',
            'sentAs': 'Content-MD5',
        }
    }
}
export const DeleteBackToSource = {
    'httpMethod': 'DELETE',
    'urlPath': 'backtosource',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        }
    }
}
export const GetBackToSource = {
    'httpMethod': 'GET',
    'urlPath': 'backtosource',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        }
    }
}
export const GetBackToSourceOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'BackToSourceConfiguration',
    },
    'parameters': {
        'BackToSourceRules': backToSourceRules
    },
}


export const SetBucketAlias = {
    'httpMethod': 'PUT',
    'urlPath': 'obsbucketalias',
    'data': {
        'xmlRoot': 'CreateBucketAlias'
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'BucketList': {
            'location': 'xml',
            'type': 'object',
            'sentAs': 'BucketList',
            'parameters': {
                'Bucket': {
                    'location': 'xml',
                    'type': 'array',
                    'items': {
                        'parameters': {
                            'sentAs': 'Bucket'
                        }
                    }
                }
            }
        }
    }
}
export const GetBucketAlias = {
    'httpMethod': 'GET',
    'urlPath': 'obsalias',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        }
    }
}
export const GetBucketAliasOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'AliasList'
    },
    'parameters': {
        'BucketAlias': {
            'location': 'xml',
            'type': "object",
            'sentAs': 'BucketAlias',
            'parameters': {
                'Alias': {
                    'sentAs': 'Alias',
                },
                'BucketList': {
                    'sentAs': 'Bucket',
                    'location': 'xml',
                    'type': 'array',
                    'wrapper': 'BucketList',
                    'items': {
                        'type': 'string',
                    }
                }
            },
        }
    }
}
export const DeleteBucketAlias = {
    'httpMethod': 'DELETE',
    'urlPath': 'obsbucketalias',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        }
    }
}
export const BindBucketAlias = {
    'httpMethod': 'PUT',
    'urlPath': 'obsalias',
    'data': {
        'xmlRoot': 'AliasList'
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'Alias': {
            'location': 'xml',
            'type': 'string',
            'sentAs': 'Alias',
        },
    }
}
export const BindBucketAliasOutput = {
    'data': {
        'xmlRoot': 'AliasList'
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'Alias': {
            'location': 'xml',
            'type': 'string',
            'sentAs': 'Alias',
        },
    }
}
export const UnbindBucketAlias = {
    'httpMethod': 'DELETE',
    'urlPath': 'obsalias',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
    }
}
export const ListBucketsAlias = {
    'httpMethod': 'GET',
    'urlPath': 'obsbucketalias',
}
export const ListBucketsAliasOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'ListBucketAliasResult'
    },
    'parameters': {
        'BucketAliasList': {
            'location': 'xml',
            'sentAs': 'BucketAliasList',
            'type': 'object',
            'parameters': {
                'BucketAlias': {
                    'location': 'xml',
                    'type': 'array',
                    'sentAs': 'BucketAlias',
                    'items': {
                        'type': 'object',
                        'parameters': {
                            'Alias': {
                                'sentAs': 'Alias',
                            },
                            'CreationDate': {
                                'sentAs': 'CreationDate',
                            },
                            'BucketList': {
                                'location': 'xml',
                                'type': 'object',
                                'sentAs': 'BucketList',
                                'parameters': {
                                    'Bucket': {
                                        'location': 'xml',
                                        'type': 'array',
                                        'items': {
                                            'parameters': {
                                                'sentAs': 'Bucket'
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        'Owner': {
            'location': 'xml',
            'sentAs': 'Owner',
            'type': 'object',
            'parameters': {
                'ID': {
                    'sentAs': 'ID',
                }
            }
        }
    }
}

export const SetBucketDisPolicy = {
    'httpMethod': 'PUT',
    'urlPath': 'disPolicy',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'ApiPath': {
            'location': 'uri',
        },
        'Rules': {
            'required': true,
            'location': 'body',
        },
    },
}

export const GetBucketDisPolicy = {
    'httpMethod': 'GET',
    'urlPath': 'disPolicy',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'ApiPath': {
            'location': 'uri',
        },
        'ContentType': {
            'location': 'header',
            'sentAs': 'Content-Type'
        }
    }
}
export const GetBucketDisPolicyOutput = {
    'data': {
        'type': 'body',
    },
    'parameters': {
        'Rules': {
            'location': 'body',
        },
    },
}

export const DeleteBucketDisPolicy = {
    'httpMethod': 'DELETE',
    'urlPath': 'disPolicy',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'ApiPath': {
            'location': 'uri',
        }
    },
}

export const GetBucketObjectLockConfiguration = {
    'httpMethod': 'GET',
    'urlPath': 'object-lock',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    }
}
export const GetBucketObjectLockConfigurationOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'ObjectLockConfiguration',
    },
    'parameters': {
        'Rule': objectLock
    }
}

export const SetBucketObjectLockConfig = {
    'httpMethod': 'PUT',
    'urlPath': 'object-lock',
    'data': {
        'type': 'xml',
        'xmlAllowEmpty': true,
        'xmlRoot': 'ObjectLockConfiguration',
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Rule': objectLock
    }
}

export const DeleteWorkflow = {
    'httpMethod': 'DELETE',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'Other_parameter': {
            'location': 'uri',
            'sentAs': 'graph_name'
        }
    }
};
export const UpdateWorkflow = {
    'httpMethod': 'PUT',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'Other_parameter': {
            'location': 'uri',
            'sentAs': 'graph_name'
        },
        'Graph_name': {
            'required': true,
            'location': 'body'
        }
    }
};
export const GetWorkflowList = {
    'httpMethod': 'GET',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'Other_parameter': {
            'location': 'uri',
            'sentAs': 'graph_name_prefix'
        },
        'XObsLimit': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'x-workflow-limit'
        },
        'XObsPrefix': {
            'location': 'urlPath',
            'sentAs': 'x-workflow-prefix',
        },
        'XObsStart': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'x-workflow-start'
        }
    }
};
export const GetWorkflowListOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'workflows': {
            'location': 'body'
        }
    }
};
export const GetWorkflowTemplateList = {
    'httpMethod': 'GET',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'Other_parameter': {
            'location': 'uri',
            'sentAs': 'template_name_prefix'
        },
        'Start': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'x-workflow-start'
        },
        'Limit': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'x-workflow-limit'
        },
        'X-workflow-prefix': {
            'location': 'urlPath',
            'sentAs': 'x-workflow-prefix',
        }
    }
};
export const GetWorkflowTemplateListOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'templates': {
            'location': 'body'
        }
    }
};
export const GetWorkflowInstanceList = {
    'httpMethod': 'GET',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'Other_parameter': {
            'location': 'uri',
            'sentAs': 'execution_name_prefix'
        },
        'Start': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'x-workflow-start'
        },
        'Limit': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'x-workflow-limit'
        },
        'Graph_name': {
            'location': 'urlPath',
            'sentAs': 'x-workflow-graph-name'
        },
        'State': {
            'location': 'urlPath',
            'sentAs': 'x-workflow-execution-state'
        },
        'X-workflow-prefix': {
            'location': 'urlPath',
            'sentAs': 'x-workflow-prefix',
        }
    }
};
export const GetWorkflowInstanceListOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'instances': {
            'location': 'body'
        }
    }
};


export const getWorkflowTrigger = (isObs) => {
    const urlPath = isObs ? "obsworkflowtriggerpolicy" : "triggerpolicy";
    return ({
        'httpMethod': 'GET',
        urlPath,
        'parameters': {
            'Bucket': {
                'required': true,
                'location': 'uri'
            },
            'ApiPath': {
                'location': 'uri'
            }
        }
    })
}

export const GetWorkflowTriggerOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'rules': {
            'location': 'body'
        }
    }
}

export const deleteWorkflowTrigger = (isObs) => {
    const urlPath = isObs ? "obsworkflowtriggerpolicy" : "triggerpolicy";
    return ({
        'httpMethod': 'DELETE',
        urlPath,
        'parameters': {
            'Bucket': {
                'required': true,
                'location': 'uri'
            },
            'ApiPath': {
                'location': 'uri'
            }
        }
    });

}
export const createWorkflowTrigger = (isObs) => {
    const urlPath = isObs ? "obsworkflowtriggerpolicy" : "triggerpolicy";
    return ({
        'httpMethod': 'PUT',
        urlPath,
        'parameters': {
            'Bucket': {
                'required': true,
                'location': 'uri'
            },
            'ApiPath': {
                'location': 'uri'
            },
            'Rule': {
                'required': true,
                'location': 'body',
            },
        }
    })
}

export const RestoreFailedWorkflowExecution = {
    'httpMethod': 'PUT',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'Other_parameter': {
            'required': true,
            'location': 'uri',
            'sentAs': 'execution_name'
        },
        'GraphName': {
            'required': true,
            'location': 'urlPath',
            'sentAs': 'x-workflow-graph-name'
        },
    },
}

export const CreateTemplate = {
    'httpMethod': 'POST',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'Template': {
            'required': true,
            'location': 'body',
        }
    },
}

export const CreateWorkflow = {
    'httpMethod': 'POST',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'Other_parameter': {
            'required': true,
            'location': 'uri',
            'sentAs': 'graph_name'
        },
        'Workflow_create': {
            'location': 'urlPath',
            'sentAs': 'x-workflow-create'
        },
        'Workflow': {
            'required': true,
            'location': 'body',
        }
    }
}

export const GetCDNNotifyConfiguration = {
    'httpMethod': 'GET',
    'urlPath': 'CDNNotifyConfiguration',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'NotForwardTag': {
            'type': 'string',
            'sentAs': 'x-obs-not-forward-tag',
            'location': 'header'
        },
    }
}

export const GetCDNNotifyConfigurationOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'CDNNotifyConfiguration'
    },
    'parameters': {
        'Domain': CDNNotifyConfiguration
    }
}

export const SetCDNNotifyConfiguration = {
    'httpMethod': 'PUT',
    'urlPath': 'CDNNotifyConfiguration',
    'data': {
        'xmlRoot': 'CDNNotifyConfiguration',
        'xmlAllowEmpty': true
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',

        },
        'NotForwardTag': {
            'type': 'string',
            'sentAs': 'x-obs-not-forward-tag',
            'location': 'header'
        },
        'Domain': CDNNotifyConfiguration
    },
}

export const GetQuota = {
    'httpMethod': 'GET',
    'urlPath': 'quota',
}

export const GetQuotaOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'MaxBucketNumber',
    },
    'parameters': {
        'Size': {
            'location': 'xml',
            'sentAs': 'Number',
            'type': 'number'
        }
    },
}

export const CreateAuditPolicy = {
    'httpMethod': 'POST',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'CreateReviewTask': {
            'required': true,
            'location': 'body'
        }
    }
}

export const CreateAuditPolicyOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'CreateReviewTask': {
            'location': 'body'
        }
    }
}
export const GetAuditPolicy = {
    'httpMethod': 'GET',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        }
    }
}
export const GetAuditPolicyOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'result': {
            'location': 'body'
        }
    }
}
export const PutAuditPolicy = {
    'httpMethod': 'PUT',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'CreateReviewTask': {
            'required': true,
            'location': 'body'
        }
    }
}
export const PutAuditPolicyOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'CreateReviewTask': {
            'location': 'body'
        }
    }
}
export const DeleteAuditPolicy = {
    'httpMethod': 'DELETE',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'Other_parameter': {
            'location': 'uri',
            'sentAs': 'task_name'
        }
    }
}
export const GetAuditResult = {
    'httpMethod': 'GET',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'Type': {
            'location': 'urlPath',
            'sentAs': 'type'
        },
        'Limit': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'limit'
        },
        'Offset': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'offset'
        }
    }
}
export const GetAuditResultOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'result': {
            'location': 'body'
        }
    }
}


export const DeleteTemplate = {
    'httpMethod': 'DELETE',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'Other_parameter': {
            'location': 'uri',
            'sentAs': 'template_name'
        }
    }
}
export const GetActionTemplates = {
    'httpMethod': 'GET',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'Other_parameter': {
            'location': 'uri',
            'sentAs': 'template_name_prefix'
        },
        'XObsPrefix': {
            'location': 'urlPath',
            'sentAs': 'x-workflow-prefix',
        },
        'XObsCategory': {
            'type': 'String',
            'location': 'urlPath',
            'sentAs': 'x-workflow-category'
        }
    }
}
export const GetActionTemplatesOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'templates': {
            'location': 'body'
        }
    }
}
export const GetWorkflowAuthorization = {
    'httpMethod': 'GET',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        }
    }

}
export const GetWorkflowAuthorizationOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'authorization': {
            'location': 'body'
        }
    }
}
export const OpenWorkflowAuthorization = {
    'httpMethod': 'POST',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        }
    }
}
export const CreateOnlineDecom = {
    'httpMethod': 'PUT',
    'urlPath': 'obscompresspolicy',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Decom': {
            'required': true,
            'location': 'body',
        },
    },
}
export const GetOnlineDecom = {
    'httpMethod': 'GET',
    'urlPath': 'obscompresspolicy',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}
export const GetOnlineDecomOutput = {
    'data': {
        'type': 'body',
    },
    'parameters': {
        'Decom': {
            'location': 'body',
        },
    },
}
export const DeleteOnlineDecom = {
    'httpMethod': 'DELETE',
    'urlPath': 'obscompresspolicy',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}
export const GetPublicationTemplates = {
    'httpMethod': 'GET',
    'parameters': templates
}
export const GetPublicationTemplatesOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'PublishedTemplates': {
            'location': 'body'
        }
    }
}
export const GetPublicationTemplateDetail = {
    'httpMethod': 'GET',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'OtherParameter': {
            'location': 'uri',
            'sentAs': 'template_name'
        }
    }
}
export const GetPublicationTemplateDetailOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'PublishTemplate': {
            'location': 'body'
        }
    }
}
export const GetWorkflowAgreements = {
    'httpMethod': 'GET',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'XWorkflowType': {
            'required': true,
            'location': 'urlPath',
            'sentAs': 'x-workflow-type'
        }
    }

}
export const GetWorkflowAgreementsOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'authorization': {
            'location': 'body'
        }
    }
}
export const OpenWorkflowAgreements = {
    'httpMethod': 'POST',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'XWorkflowType': {
            'required': true,
            'location': 'urlPath',
            'sentAs': 'x-workflow-type'
        }
    }
}
export const CreateMyActionTemplate = {
    'httpMethod': 'POST',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'OtherParameter': {
            'location': 'uri',
            'sentAs': 'template_name'
        },
        'ActionTemplate': {
            'required': true,
            'location': 'body',
        }
    }
}

export const CreateMyActionTemplateOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'ActionTemplate': {
            'location': 'body'
        }
    }
}


export const GetMyActionTemplates = {
    'httpMethod': 'GET',
    'parameters': templates
}

export const GetMyActionTemplatesOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'ActionTemplates': {
            'location': 'body'
        }
    }
}
export const GetMyactiontemplateDetail = {
    'httpMethod': 'GET',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'OtherParameter': {
            'location': 'uri',
            'sentAs': 'template_name'
        }
    }
}
export const GetMyactiontemplateDetailOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'ActionTemplate': {
            'location': 'body'
        }
    }
}
export const UpdateMyActionTemplate = {
    'httpMethod': 'PUT',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'OtherParameter': {
            'location': 'uri',
            'sentAs': 'template_name'
        },
        'ActionTemplate': {
            'required': true,
            'location': 'body',
        }
    }
}
export const UpdateMyActionTemplateOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'ActionTemplate': {
            'location': 'body'
        }
    }
}
export const DeleteMyActionTemplate = {
    'httpMethod': 'DELETE',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'OtherParameter': {
            'location': 'uri',
            'sentAs': 'template_name'
        }
    }
}
export const ForbidMyActionTemplate = {
    'httpMethod': 'DELETE',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'OtherParameter': {
            'location': 'uri',
            'sentAs': 'template_name'
        },
        'XObsForbid': {
            'location': 'urlPath',
            'sentAs': 'x-workflow-forbid'
        }
    }
}
export const UpdatePublicActionTemplate = {
    'httpMethod': 'PUT',
    'parameters': {
        'ApiPath': {
            'location': 'uri'
        },
        'OtherParameter': {
            'location': 'uri',
            'sentAs': 'template_name'
        },
        'PublicAction': {
            'required': true,
            'location': 'body',
        }
    }
}
export const GetOmPublicActionTemplates = {
    'httpMethod': 'GET',
    'parameters': templates
}
export const GetOmPublicActionTemplatesOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'Templates': {
            'location': 'body'
        }
    }
}