/**
 * Copyright 2019 Huawei Technologies Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 */

import {
    getLocationXML,
    getStorageClassHeader
} from "./baseModel";

export const createSfsBucket = isObs => {
    return (
        {
            'httpMethod': 'PUT',
            'data': {
                'xmlRoot': 'CreateBucketConfiguration'
            },
            'parameters': {
                'ApiPath': {
                    'location': 'uri',
                },
                'Acl': {
                    'location': 'header',
                    'sentAs': 'acl',
                    'withPrefix': true
                },
                'StorageType': {
                    'location': 'header',
                    'sentAs': getStorageClassHeader(isObs),
                },
                'ObjectLockEnabeld': {
                    'location': 'header',
                    'sentAs': 'bucket-object-lock-enabled',
                    'withPrefix': true
                },
                'IESLocation': {
                    'location': 'header',
                    'sentAs': 'ies-location',
                    'withPrefix': true
                },
                'FileInterface': {
                    'location': 'header',
                    'sentAs': 'fs-file-interface',
                    'withPrefix': true
                },
                'Type': {
                    'location': 'header',
                    'sentAs': 'bucket-type',
                    'withPrefix': true
                },
                'MultiAz': {
                    'location': 'header',
                    'sentAs': 'az-redundancy',
                    'withPrefix': true
                },
                'Redundancy': {
                    'location': 'header',
                    'sentAs': 'bucket-redundancy',
                    'withPrefix': true
                },
                'IsFusionAllowUpgrade': {
                    'location': 'header',
                    'sentAs': 'fusion-allow-upgrade',
                    'withPrefix': true
                },
                'IsFusionAllowAlternative': {
                    'location': 'header',
                    'sentAs': 'fusion-allow-alternative',
                    'withPrefix': true
                },
                'Cluster': {
                    'location': 'header',
                    'sentAs': 'location-clustergroup-id',
                    'withPrefix': true
                },
                'GrantFullControl': {
                    'location': 'header',
                    'sentAs': 'grant-full-control',
                    'withPrefix': true
                },
                'GrantRead': {
                    'location': 'header',
                    'sentAs': 'grant-read',
                    'withPrefix': true
                },
                'GrantReadACP': {
                    'location': 'header',
                    'sentAs': 'grant-read-acp',
                    'withPrefix': true
                },
                'GrantWrite': {
                    'location': 'header',
                    'sentAs': 'grant-write',
                    'withPrefix': true
                },
                'GrantWriteACP': {
                    'location': 'header',
                    'sentAs': 'grant-write-acp',
                    'withPrefix': true
                },
                'ClusterType': {
                    'location': 'header',
                    'sentAs': 'cluster-type',
                    'withPrefix': true
                },
                "MultiEnterprise": { "location": "header", 'sentAs': "epid", 'withPrefix': true },
                "Location": {
                    "location": "xml",
                    "sentAs": getLocationXML(isObs)
                }
            }
        }
    );
}

export const DeleteSfsBucket = {
    'httpMethod': 'DELETE',
    'parameters': {
        'ApiPath': {
            'location': 'uri',
        },
    },
};

export const SetSFSAcl = {
    'httpMethod': 'PUT',
    'urlPath': 'sfsacl',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Policy': {
            'required': true,
            'location': 'body',
        },
    },
}

export const GetSFSAcl = {
    'httpMethod': 'GET',
    'urlPath': 'sfsacl',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}

export const GetSFSAclOutput = {
    'data': {
        'type': 'body',
    },
    'parameters': {
        'Policy': {
            'location': 'body',
        },
    },
}

export const DeleteSFSAcl = {
    'httpMethod': 'DELETE',
    'urlPath': 'sfsacl',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}

export const GetSFSPermissionAcl = {
    'httpMethod': "GET",
    'parameters': {
        'ApiPath': {
            'location': "uri",
        },
    },
};

export const GetSFSPermissionAclOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'PermissionGroup': {
            'location': "body",
        },
    },
};

export const UpdateSFSPermissionAcl = {
    'httpMethod': "PUT",
    'parameters': {
        'ApiPath': {
            'location': "uri",
        },
        'Params': {
            'location': "body",
        },
    },
};

export const DeleteSFSPermissionAcl = {
    'httpMethod': "DELETE",
    'parameters': {
        'ApiPath': {
            'location': "uri",
        },
    },
};

export const GetSFSPermissionGroupList = {
    'httpMethod': "GET",
    'parameters': {
        'ApiPath': {
            'location': "uri",
        },
        'Limit': {
            'location': "urlPath",
            'sentAs': "limit",
        },
        'Offset': {
            'location': "urlPath",
            'sentAs': "offset",
        },
    },
};

export const GetSFSPermissionGroupListOutput = {
    'data': {
        'type': "body",
    },
    'parameters': {
        'PermissionGroups': {
            'location': "body",
        },
    },
};

export const SetSFSPermissionGroup = {
    'httpMethod': "POST",
    'parameters': {
        'ApiPath': {
            'location': "uri",
        },
        'Params': {
            'location': "body",
        },
    },
};

export const UpdateSFSPermissionGroup = {
    'httpMethod': "PUT",
    'parameters': {
        'ApiPath': {
            'location': "uri",
        },
        'Params': {
            'location': "body",
        },
    },
};

export const GetSFSPermissionGroup = {
    'httpMethod': "GET",
    'parameters': {
        'ApiPath': {
            'location': "uri",
        },
    },
};

export const GetSFSPermissionGroupOutput = {
    'data': {
        'type': "body",
    },
    'parameters': {
        'PermissionGroup': {
            'location': "body",
        },
    },
};

export const DeleteSFSPermissionGroup = {
    'httpMethod': "DELETE",
    'parameters': {
        'ApiPath': {
            'location': "uri",
        },
    },
};
