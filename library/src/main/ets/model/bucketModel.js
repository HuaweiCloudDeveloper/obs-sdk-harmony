/**
 * Copyright 2019 Huawei Technologies Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 */

import {
    getLocationHeader,
    getStorageClassHeader,

    owner,
    bucketEncryptionRule,
    commonPrefixes,
    corsRule,
    customDomainBody,
    inventoryConfiguration,
    loggingEnabled,
    getLocationXML,
    grants,
    tagSet,
    topicConfiguration,
    functionGraphConfiguration,
    functionStageConfiguration,
    eventGridConfiguration,
    replicationRules,
    lifecycleRule,
    redirectAllRequestsTo,
    indexDocument,
    errorDocument,
    routingRules,
    optionOutput,
    baseOptionInput,
} from "./baseModel";

export const HeadBucket = {
    'httpMethod': 'HEAD',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        }
    }
}

export const HeadApiVersion = {
    'httpMethod': 'HEAD',
    'urlPath': 'apiversion',
    'parameters': {
        'Bucket': {
            'location': 'uri',
        },
    },
}

export const HeadApiVersionOutput = {
    'parameters': {
        'ApiVersion': {
            'location': 'header',
            'sentAs': 'x-obs-api'
        }
    }
}

export const createBucket = (isObs) => ({
    'httpMethod': 'PUT',
    'data': {
        'xmlRoot': 'CreateBucketConfiguration'
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'Acl': {
            'location': 'header',
            'sentAs': 'acl',
            'withPrefix': true
        },
        'StorageClass': {
            'location': 'header',
            'sentAs': getStorageClassHeader(isObs)
        },
        'IESLocation': {
            'location': 'header',
            'sentAs': 'ies-location',
            'withPrefix': true
        },
        'ObjectLockEnabeld': {
            'location': 'header',
            'sentAs': 'bucket-object-lock-enabled',
            'withPrefix': true
        },
        'FileInterface': {
            'location': 'header',
            'sentAs': 'fs-file-interface',
            'withPrefix': true
        },
        'BucketType': {
            'location': 'header',
            'sentAs': 'bucket-type',
            'withPrefix': true
        },
        'AzRedundancy': {
            'location': 'header',
            'sentAs': 'x-obs-az-redundancy'
        },
        'Redundancy': {
            'location': 'header',
            'sentAs': 'bucket-redundancy',
            'withPrefix': true
        },
        'IsFusionAllowUpgrade': {
            'location': 'header',
            'sentAs': 'fusion-allow-upgrade',
            'withPrefix': true
        },
        'IsFusionAllowAlternative': {
            'location': 'header',
            'sentAs': 'fusion-allow-alternative',
            'withPrefix': true
        },
        'Cluster': {
            'location': 'header',
            'sentAs': 'location-clustergroup-id',
            'withPrefix': true
        },
        'GrantFullControl': {
            'location': 'header',
            'sentAs': 'grant-full-control',
            'withPrefix': true
        },
        'GrantFullControlDelivered': {
            'location': 'header',
            'sentAs': 'grant-full-control-delivered',
            'withPrefix': true
        },
        'GrantRead': {
            'location': 'header',
            'sentAs': 'grant-read',
            'withPrefix': true
        },
        'GrantReadDelivered': {
            'location': 'header',
            'sentAs': 'grant-read-delivered',
            'withPrefix': true
        },
        'GrantReadACP': {
            'location': 'header',
            'sentAs': 'grant-read-acp',
            'withPrefix': true
        },
        'GrantWrite': {
            'location': 'header',
            'sentAs': 'grant-write',
            'withPrefix': true
        },
        'GrantWriteACP': {
            'location': 'header',
            'sentAs': 'grant-write-acp',
            'withPrefix': true
        },
        'ClusterType': {
            'location': 'header',
            'sentAs': 'cluster-type',
            'withPrefix': true
        },
        "Epid": {
            "location": "header",
            'sentAs': "epid",
            'withPrefix': true
        },
        "Location": {
            "location": "xml",
            "sentAs": getLocationXML(isObs)
        },
        ...bucketEncryptionRule
    }
});

export const GetBucketMetadata = {
    'httpMethod': 'HEAD',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Origin': {
            'location': 'header',
            'sentAs': 'Origin'
        },
        'RequestHeader': {
            'location': 'header',
            'sentAs': 'Access-Control-Request-Headers'
        }
    },
};

export const getBucketMetadataOutput = (isObs) => ({
    'parameters': {
        'StorageClass': {
            'location': 'header',
            'sentAs': getStorageClassHeader(isObs),
        },
        'ObsVersion': {
            'location': 'header',
            'sentAs': 'version',
            'withPrefix': true
        },
        'Location': {
            'location': 'header',
            'sentAs': getLocationHeader(isObs),
            'withPrefix': true
        },
        'FileInterface': {
            'location': 'header',
            'sentAs': 'fs-file-interface',
            'withPrefix': true
        },
        'BucketType': {
            'location': 'header',
            'sentAs': 'bucket-type',
            'withPrefix': true
        },
        'AzRedundancy': {
            "location": 'header',
            'sentAs': 'az-redundancy',
            'withPrefix': true
        },
        'Redundancy': {
            'location': 'header',
            'sentAs': 'bucket-redundancy',
            'withPrefix': true
        },
        'Cluster': {
            'location': 'header',
            'sentAs': 'location-clustergroup-id',
            'withPrefix': true
        },
        'Epid': {
            'location': 'header',
            'sentAs': 'epid',
            'withPrefix': true
        },
        'ClusterType': {
            "location": 'header',
            'sentAs': 'cluster-type',
            'withPrefix': true
        },
        'IESLocation': {
            'location': 'header',
            'sentAs': 'ies-location',
            'withPrefix': true
        },
        'AllowOrigin': {
            'location': 'header',
            'sentAs': 'access-control-allow-origin'
        },
        'MaxAgeSeconds': {
            'location': 'header',
            'sentAs': 'access-control-max-age',
            'type': 'number',
        },
        'ExposeHeader': {
            'location': 'header',
            'sentAs': 'access-control-expose-headers'
        },
        'AllowMethod': {
            'location': 'header',
            'sentAs': 'access-control-allow-methods'
        },
        'AllowHeader': {
            'location': 'header',
            'sentAs': 'access-control-allow-headers'
        }
    }
});

export const ListObjects = {
    'httpMethod': 'GET',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Prefix': {
            'location': 'urlPath',
            'sentAs': 'prefix',
        },
        'Marker': {
            'location': 'urlPath',
            'sentAs': 'marker',
        },
        'MaxKeys': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'max-keys',
        },
        'Delimiter': {
            'location': 'urlPath',
            'sentAs': 'delimiter',
        },
        'EncodingType': {
            'location': 'urlPath',
            'sentAs': 'encoding-type'
        },
    },
};

export const listObjectsOutput = isObs => {
    return ({
        'data': {
            'type': 'xml',
            'xmlRoot': 'ListBucketResult',
        },
        'parameters': {
            'Location': {
                'location': 'header',
                'sentAs': getLocationHeader(isObs),
                'withPrefix': true
            },
            'Bucket': {
                'location': 'xml',
                'sentAs': 'Name',
            },
            'Delimiter': {
                'decode': true,
                'location': 'xml',
                'sentAs': 'Delimiter',
            },
            'IsTruncated': {
                'location': 'xml',
                'sentAs': 'IsTruncated',
                'type': 'boolean',
            },
            'Prefix': {
                'decode': true,
                'location': 'xml',
                'sentAs': 'Prefix',
            },
            'Marker': {
                'decode': true,
                'location': 'xml',
                'sentAs': 'Marker',
            },
            'NextMarker': {
                'decode': true,
                'location': 'xml',
                'sentAs': 'NextMarker',
            },
            'MaxKeys': {
                'location': 'xml',
                'sentAs': 'MaxKeys',
                'type': 'number',
            },
            'EncodingType': {
                'location': 'xml',
                'sentAs': 'EncodingType',
            },
            'Contents': {
                'type': 'array',
                'location': 'xml',
                'sentAs': 'Contents',
                'items': {
                    'type': 'object',
                    'parameters': {
                        'Key': {
                            'decode': true,
                            'sentAs': 'Key',
                        },
                        'LastModified': {
                            'sentAs': 'LastModified',
                        },
                        'ETag': {
                            'sentAs': 'ETag',
                        },
                        'Size': {
                            'sentAs': 'Size',
                            'type': 'number',
                        },
                        'Type': {
                            'sentAs': 'Type'
                        },
                        'StorageClass': {
                            'sentAs': 'StorageClass',
                        },
                        'Owner': owner
                    },
                },

            },
            'CommonPrefixes': commonPrefixes
        },
    })
};

export const ListVersions = {
    'httpMethod': 'GET',
    'urlPath': 'versions',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Prefix': {
            'location': 'urlPath',
            'sentAs': 'prefix',
        },
        'KeyMarker': {
            'location': 'urlPath',
            'sentAs': 'key-marker',
        },
        'MaxKeys': {
            'type': 'number',
            'location': 'urlPath',
            'sentAs': 'max-keys',
        },
        'Delimiter': {
            'location': 'urlPath',
            'sentAs': 'delimiter',
        },
        'VersionIdMarker': {
            'location': 'urlPath',
            'sentAs': 'version-id-marker',
        },
        'EncodingType': {
            'location': 'urlPath',
            'sentAs': 'encoding-type',
        },
    },
};
export const listVersionsOutput = isObs => {
    return ({
        'data': {
            'type': 'xml',
            'xmlRoot': 'ListVersionsResult',
        },
        'parameters': {
            'EncodingType': {
                'location': 'xml',
                'sentAs': 'EncodingType',
            },
            'Location': {
                'location': 'header',
                'sentAs': getLocationHeader(isObs),
                'withPrefix': true
            },
            'Bucket': {
                'location': 'xml',
                'sentAs': 'Name',
            },
            'Prefix': {
                'decode': true,
                'location': 'xml',
                'sentAs': 'Prefix',
            },
            'Delimiter': {
                'decode': true,
                'location': 'xml',
                'sentAs': 'Delimiter',
            },
            'KeyMarker': {
                'decode': true,
                'location': 'xml',
                'sentAs': 'KeyMarker',
            },
            'VersionIdMarker': {
                'location': 'xml',
                'sentAs': 'VersionIdMarker',
            },
            'NextKeyMarker': {
                'decode': true,
                'location': 'xml',
                'sentAs': 'NextKeyMarker',
            },
            'NextVersionIdMarker': {
                'location': 'xml',
                'sentAs': 'NextVersionIdMarker',
            },
            'MaxKeys': {
                'location': 'xml',
                'sentAs': 'MaxKeys',
                'type': 'number',
            },
            'IsTruncated': {
                'location': 'xml',
                'sentAs': 'IsTruncated',
                'type': 'boolean',
            },
            'Versions': {
                'type': 'array',
                'location': 'xml',
                'sentAs': 'Version',
                'items': {
                    'type': 'object',
                    'parameters': {
                        'Key': {
                            'decode': true,
                            'sentAs': 'Key',
                        },
                        'VersionId': {
                            'sentAs': 'VersionId',
                        },
                        'IsLatest': {
                            'sentAs': 'IsLatest',
                            'type': 'boolean'
                        },
                        'LastModified': {
                            'sentAs': 'LastModified',
                        },
                        'ETag': {
                            'sentAs': 'ETag',
                        },
                        'Size': {
                            'sentAs': 'Size',
                            'type': 'number'
                        },
                        'Type': {
                            'sentAs': 'Type'
                        },
                        'Owner': owner,
                        'StorageClass': {
                            'sentAs': 'StorageClass',
                        }
                    }
                },
            },
            'DeleteMarkers': {
                'type': 'array',
                'location': 'xml',
                'sentAs': 'DeleteMarker',
                'items': {
                    'type': 'object',
                    'parameters': {
                        'Key': {
                            'decode': true,
                            'sentAs': 'Key',
                        },
                        'VersionId': {
                            'sentAs': 'VersionId',
                        },
                        'IsLatest': {
                            'sentAs': 'IsLatest',
                            'type': 'boolean'
                        },
                        'LastModified': {
                            'sentAs': 'LastModified',
                        },
                        'Owner': owner
                    }
                },
            },
            'CommonPrefixes': commonPrefixes
        },
    })
};

export const DeleteBucket = {
    'httpMethod': 'DELETE',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
};

export const ListBuckets = {
    'httpMethod': 'GET',
    "parameters": {
        "BucketType": {
            'sentAs': "x-obs-bucket-type",
            "location": "header"
        },
        "QueryLocation": {
            "sentAs": "location",
            "location": "header",
            "withPrefix": true
        }
    }
};

export const ListBucketsOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'ListAllMyBucketsResult'
    },
    'parameters': {
        Buckets: {
            location: "xml",
            sentAs: "Bucket",
            type: "array",
            wrapper: 'Buckets',
            items: {
                type: "object",
                location: "xml",
                sentAs: "Bucket",
                parameters: {
                    Name: {
                        sentAs: "Name"
                    },
                    CreationDate: {
                        sentAs: "CreationDate"
                    },
                    Location: {
                        sentAs: "Location"
                    },
                    ClusterType: {
                        sentAs: "ClusterType"
                    },
                    IESLocation: {
                        sentAs: "IESLocation"
                    },
                    BucketType: {
                        sentAs: "BucketType"
                    }
                }
            }
        },
        'Owner': owner
    }
};

export const GetBucketLocation = {
    'httpMethod': 'GET',
    'urlPath': 'location',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
};

export const getBucketLocationOutput = isObs => {
    const data = isObs ? {
        'type': 'xml'
    } : {
        'type': 'xml',
        'xmlRoot': 'CreateBucketConfiguration',
    };

    return ({
        'data': data,
        'parameters': {
            'Location': {
                'location': 'xml',
                'sentAs': getLocationXML(isObs),
            },
        },
    })
}

export const setBucketAcl = isObs => ({
    'httpMethod': 'PUT',
    'urlPath': 'acl',
    'data': {
        'xmlRoot': 'AccessControlPolicy',
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Acl': {
            'location': 'header',
            'sentAs': 'acl',
            'withPrefix': true,
            'type': 'adapter'
        },
        'Owner': owner,
        'Grants': grants(isObs)
    },
})

export const GetBucketAcl = {
    'httpMethod': 'GET',
    'urlPath': 'acl',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}

export const getBucketAclOutput = isObs => ({
    'data': {
        'type': 'xml',
        'xmlRoot': 'AccessControlPolicy',
    },
    'parameters': {
        'Owner': owner,
        'Grants': grants(isObs)
    }
});

export const SetBucketVersioningConfiguration = {
    'httpMethod': 'PUT',
    'urlPath': 'versioning',
    'data': {
        'xmlRoot': 'VersioningConfiguration',
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'VersionStatus': {
            'required': true,
            'location': 'xml',
            'sentAs': 'Status',
        },
    },
}

export const GetBucketVersioningConfiguration = {
    'httpMethod': 'GET',
    'urlPath': 'versioning',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}

export const GetBucketVersioningConfigurationOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'VersioningConfiguration',
    },
    'parameters': {
        'VersionStatus': {
            'location': 'xml',
            'sentAs': 'Status',
        },
    },
}


export const SetBucketCors = {
    'httpMethod': 'PUT',
    'urlPath': 'cors',
    'data': {
        'xmlRoot': 'CORSConfiguration',
        'md5': true
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'CorsRules': corsRule
    },
}

export const GetBucketCors = {
    'httpMethod': 'GET',
    'urlPath': 'cors',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}
export const GetBucketCorsOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'CORSConfiguration',
    },
    'parameters': {
        'CorsRules': corsRule
    },
}
export const DeleteBucketCors = {
    'httpMethod': 'DELETE',
    'urlPath': 'cors',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}

export const SetBucketInventory = {
    'httpMethod': 'PUT',
    'urlPath': 'inventory',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Id': {
            'location': 'urlPath',
            'sentAs': 'id'
        },
        'inventoryConfiguration': inventoryConfiguration

    },
}
export const SetBucketInventoryOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'inventoryConfiguration',
    },
    'parameters': {
        'inventoryConfiguration': inventoryConfiguration
    },

}

export const GetBucketInventory = {
    'httpMethod': 'GET',
    'urlPath': 'inventory',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}
export const GetBucketInventoryOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'ListinventoryConfiguration',
    },
    'parameters': {
        'Rules': {
            'type': 'array',
            'location': 'xml',
            'sentAs': 'inventoryConfiguration',
            'items': inventoryConfiguration,
        },
    }
}

export const DeleteBucketInventory = {
    'httpMethod': 'DELETE',
    'urlPath': 'inventory',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'Id': {
            'location': 'urlPath',
            'sentAs': 'id'
        }
    }
}
export const DeleteBucketInventoryOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'inventoryConfiguration',
    },
    'parameters': {
        'inventoryConfiguration': inventoryConfiguration
    },

}

export const setBucketLoggingConfiguration = isObs => ({
    'httpMethod': 'PUT',
    'urlPath': 'logging',
    'data': {
        'xmlRoot': 'BucketLoggingStatus',
        'xmlAllowEmpty': true
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Agency': {
            'location': 'xml',
            'sentAs': 'Agency',
        },
        'LoggingEnabled': loggingEnabled(isObs),
    },
})

export const GetBucketLoggingConfiguration = {
    'httpMethod': 'GET',
    'urlPath': 'logging',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}

export const getBucketLoggingConfigurationOutput = isObs => ({
    'data': {
        'type': 'xml',
        'xmlRoot': 'BucketLoggingStatus',
    },
    'parameters': {
        'Agency': {
            'location': 'xml',
            'sentAs': 'Agency'
        },
        'LoggingEnabled': loggingEnabled(isObs),
    },
})

export const SetBucketTagging = {
    'httpMethod': 'PUT',
    'urlPath': 'tagging',
    'data': {
        'xmlRoot': 'Tagging',
        'md5': true
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'Tags': tagSet
    }
}

export const DeleteBucketTagging = {
    'httpMethod': 'DELETE',
    'urlPath': 'tagging',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    }
}

export const GetBucketTagging = {
    'httpMethod': 'GET',
    'urlPath': 'tagging',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    }
}

export const GetBucketTaggingOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'Tagging',
    },
    'parameters': {
        'Tags': tagSet
    }
}

export const GetBucketNotification = {
    'httpMethod': 'GET',
    'urlPath': 'notification',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    }
}

export const GetBucketNotificationOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'NotificationConfiguration',
    },
    'parameters': {
        'RequestId': {
            'location': 'header',
            'sentAs': 'request-id',
            "withPrefix": true
        },
        'TopicConfigurations': topicConfiguration,
        'FunctionGraphConfigurations': functionGraphConfiguration,
        'FunctionStageConfigurations': functionStageConfiguration,
        'EventGridConfigurations': eventGridConfiguration
    },
}


export const SetBucketReplication = {
    'httpMethod': 'PUT',
    'urlPath': 'replication',
    'data': {
        'xmlRoot': 'ReplicationConfiguration',
        'md5': true
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'Agency': {
            'location': 'xml',
            'sentAs': 'Agency'
        },
        'Rules': replicationRules
    }
}

export const GetBucketReplication = {
    'httpMethod': 'GET',
    'urlPath': 'replication',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    }
}

export const GetBucketReplicationOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'ReplicationConfiguration',
    },
    'parameters': {
        'Agency': {
            'location': 'xml',
            'sentAs': 'Agency'
        },
        'Rules': replicationRules
    }
}

export const DeleteBucketReplication = {
    'httpMethod': 'DELETE',
    'urlPath': 'replication',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    }
}

export const SetBucketNotification = {
    'httpMethod': 'PUT',
    'urlPath': 'notification',
    'data': {
        'xmlRoot': 'NotificationConfiguration',
        'xmlAllowEmpty': true
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'TopicConfigurations': topicConfiguration,
        'FunctionGraphConfigurations': functionGraphConfiguration,
        'FunctionStageConfigurations': functionStageConfiguration,
        'EventGridConfigurations': eventGridConfiguration
    }
}


export const SetBucketLifecycleConfiguration = {
    'httpMethod': 'PUT',
    'urlPath': 'lifecycle',
    'data': {
        'xmlRoot': 'LifecycleConfiguration',
        'md5': true
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Rules': lifecycleRule
    },
}

export const GetBucketLifecycleConfiguration = {
    'httpMethod': 'GET',
    'urlPath': 'lifecycle',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}

export const GetBucketLifecycleConfigurationOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'LifecycleConfiguration',
    },
    'parameters': {
        'Rules': lifecycleRule
    },
}

export const DeleteBucketLifecycleConfiguration = {
    'httpMethod': 'DELETE',
    'urlPath': 'lifecycle',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}


export const SetBucketPolicy = {
    'httpMethod': 'PUT',
    'urlPath': 'policy',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'Policy': {
            'required': true,
            'location': 'body',
        },
    },
}

export const GetBucketPolicy = {
    'httpMethod': 'GET',
    'urlPath': 'policy',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}

export const GetBucketPolicyOutput = {
    'data': {
        'type': 'body',
    },
    'parameters': {
        'Policy': {
            'location': 'body',
        },
    },
}

export const DeleteBucketPolicy = {
    'httpMethod': 'DELETE',
    'urlPath': 'policy',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}



export const SetBucketWebsiteConfiguration = {
    'httpMethod': 'PUT',
    'urlPath': 'website',
    'data': {
        'xmlRoot': 'WebsiteConfiguration',
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'RedirectAllRequestsTo': redirectAllRequestsTo,
        'IndexDocument': indexDocument,
        'ErrorDocument': errorDocument,
        'RoutingRules': routingRules
    },
}

export const GetBucketWebsiteConfiguration = {
    'httpMethod': 'GET',
    'urlPath': 'website',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}

export const GetBucketWebsiteConfigurationOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'WebsiteConfiguration',
    },
    'parameters': {
        'RedirectAllRequestsTo': redirectAllRequestsTo,
        'IndexDocument': indexDocument,
        'ErrorDocument': errorDocument,
        'RoutingRules': routingRules,
    },
}

export const DeleteBucketWebsiteConfiguration = {
    'httpMethod': 'DELETE',
    'urlPath': 'website',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}

export const GetBucketEncryption = {
    'httpMethod': 'GET',
    'urlPath': 'encryption',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        }
    }
}

export const GetBucketEncryptionOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'ServerSideEncryptionConfiguration'
    },
    'parameters': {
        'Rule': bucketEncryptionRule
    }
}

export const SetBucketEncryption = {
    'httpMethod': 'PUT',
    'urlPath': 'encryption',
    'data': {
        'xmlRoot': 'ServerSideEncryptionConfiguration'
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'Rule': bucketEncryptionRule
    }
}

export const SetBucketEncryptionOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'ServerSideEncryptionConfiguration'
    },
    'parameters': {
        'Rule': bucketEncryptionRule
    }
}

export const DeleteBucketEncryption = {
    'httpMethod': 'DELETE',
    'urlPath': 'encryption',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        }
    }
}
export const DeleteBucketEncryptionOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'ServerSideEncryptionConfiguration'
    },
    'parameters': {
        'Rule': bucketEncryptionRule
    }
}


export const OptionsBucket = {
    'httpMethod': 'OPTIONS',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        ...baseOptionInput,
    },
}

export const OptionsBucketOutput = {
    'parameters': optionOutput
}

export const GetBucketRequesterPay = {
    'httpMethod': 'GET',
    'urlPath': 'requestPayment',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        }
    }
}
export const GetBucketRequesterPayOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'RequestPaymentConfiguration'
    },
    'parameters': {
        'Payer': {
            'location': 'xml',
            'sentAs': 'Payer',
        }
    }
}
export const SetBucketRequesterPay = {
    'httpMethod': 'PUT',
    'urlPath': 'requestPayment',
    'data': {
        'xmlRoot': 'RequestPaymentConfiguration'
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'Payer': {
            'location': 'xml',
            'sentAs': 'Payer'
        }
    }
}
export const SetBucketRequesterPayOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'RequestPaymentConfiguration'
    },
    'parameters': {
        'Payer': {
            'location': 'xml',
            'sentAs': 'Payer',
        }
    }
}

export const SetBucketMirrorBackToSource = {
    'httpMethod': 'PUT',
    'urlPath': 'mirrorBackToSource',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'ApiPath': {
            'location': 'uri'
        },
        'Rules': {
            'required': true,
            'location': 'body'
        },
    },
}

export const SetBucketMirrorBackToSourceOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'Rules': {
            'location': 'body'
        }
    }
}

export const GetBucketMirrorBackToSource = {
    'httpMethod': 'GET',
    'urlPath': 'mirrorBackToSource',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'ApiPath': {
            'location': 'uri'
        }
    }
}

export const GetBucketMirrorBackToSourceOutput = {
    'data': {
        'type': 'body'
    },
    'parameters': {
        'Rules': {
            'location': 'body'
        }
    }
}

export const DeleteBucketMirrorBackToSource = {
    'httpMethod': 'DELETE',
    'urlPath': 'mirrorBackToSource',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'ApiPath': {
            'location': 'uri'
        }
    }
}


export const GetBucketDirectColdAccess = {
    'httpMethod': 'GET',
    'urlPath': 'directcoldaccess',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        }
    }
}
export const GetBucketDirectColdAccessOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'DirectColdAccessConfiguration'
    },
    'parameters': {
        'Status': {
            'location': 'xml',
            'sentAs': 'Status',
        }
    }
}

export const SetBucketDirectColdAccess = {
    'httpMethod': 'PUT',
    'urlPath': 'directcoldaccess',
    'data': {
        'xmlRoot': 'DirectColdAccessConfiguration',
        'md5': true
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'Status': {
            'required': true,
            'location': 'xml',
            'sentAs': 'Status'
        }
    }
}

export const SetBucketDirectColdAccessOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'DirectColdAccessConfiguration'
    },
    'parameters': {
        'Status': {
            'location': 'xml',
            'sentAs': 'Status'
        }
    }
}

export const DeleteBucketDirectColdAccess = {
    'httpMethod': 'DELETE',
    'urlPath': 'directcoldaccess',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        }
    }
}
export const DeleteBucketDirectColdAccessOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'DirectColdAccessConfiguration'
    },
    'parameters': {
        'Status': {
            'location': 'xml',
            'sentAs': 'Status'
        }
    }
}

export const GetBucketCustomDomain = {
    'httpMethod': 'GET',
    'urlPath': 'customdomain',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        }
    }
}
export const GetBucketCustomDomainOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'ListBucketCustomDomainsResult'
    },
    'parameters': {
        'Domains': {
            'location': 'xml',
            'type': 'array',
            'sentAs': 'Domains',
            'items': {
                'type': 'object',
                'parameters': {
                    'DomainName': {
                        'sentAs': 'DomainName',
                    },
                    'Value': {
                        'sentAs': 'CreateTime',
                    },
                    'CertificateId': {
                        'sentAs': 'CertificateId',
                    }
                }
            }
        }
    }
}
export const SetBucketCustomDomain = {
    'httpMethod': 'PUT',
    'data': {
        'xmlRoot': 'CustomDomainConfiguration',
        'md5': true
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'DomainName': {
            'location': 'urlPath',
            'sentAs': 'customdomain'
        },
        ...customDomainBody
    }
}

export const DeleteBucketCustomDomain = {
    'httpMethod': 'DELETE',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri'
        },
        'DomainName': {
            'location': 'urlPath',
            'sentAs': 'customdomain'
        }
    }
}


export const SetBucketQuota = {
    'httpMethod': 'PUT',
    'urlPath': 'quota',
    'data': {
        'xmlRoot': 'Quota',
    },
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
        'StorageQuota': {
            'required': true,
            'location': 'xml',
            'sentAs': 'StorageQuota',
            'type': "number",
        },
        'NumberQuota': {
            'required': false,
            'location': 'xml',
            'sentAs': 'NumberQuota',
            'type': "number",
        },
    },
}

export const GetBucketQuota = {
    'httpMethod': 'GET',
    'urlPath': 'quota',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },

    },
}
export const GetBucketQuotaOutput = {
    'data': {
        'type': 'xml',
        'xmlRoot': 'Quota',
    },
    'parameters': {
        'StorageQuota': {
            'location': 'xml',
            'sentAs': 'StorageQuota',
            'type': 'number'
        },
        'NumberQuota': {
            'location': 'xml',
            'sentAs': 'NumberQuota',
            'type': 'number'
        },
    },
}



export const GetBucketStorageInfo = {
    'httpMethod': 'GET',
    'urlPath': 'storageinfo',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    },
}

export const getBucketStorageInfoOutput = isObs => {
    let storageInfo = {
        'WarmSize': {
            'location': 'xml',
            'sentAs': 'WarmSize',
        },
        'WarmObjectNumber': {
            'location': 'xml',
            'sentAs': 'WarmObjectNumber',
        },
        'ColdSize': {
            'location': 'xml',
            'sentAs': 'ColdSize',
        },
        'ColdObjectNumber': {
            'location': 'xml',
            'sentAs': 'ColdObjectNumber',
        },
    }
    if (!isObs) {
        storageInfo = {
            'Standard_IASize': {
                'location': 'xml',
                'sentAs': 'Standard_IASize',
            },
            'Standard_IAObjectNumber': {
                'location': 'xml',
                'sentAs': 'Standard_IAObjectNumber',
            },
            'GlacierSize': {
                'location': 'xml',
                'sentAs': 'GlacierSize',
            },
            'GlacierObjectNumber': {
                'location': 'xml',
                'sentAs': 'GlacierObjectNumber',
            },
        }
    }
    return ({
        'data': {
            'type': 'xml',
            'xmlRoot': 'GetBucketStorageInfoResult',
        },
        'parameters': {
            'Size': {
                'location': 'xml',
                'sentAs': 'Size',
                'type': 'number'
            },
            'ObjectNumber': {
                'location': 'xml',
                'sentAs': 'ObjectNumber',
                'type': 'number'
            },
            'StandardSize': {
                'location': 'xml',
                'sentAs': 'StandardSize',
                'type': 'number'
            },
            'StandardObjectNumber': {
                'location': 'xml',
                'sentAs': 'StandardObjectNumber',
                'type': 'number'
            },
            ...storageInfo,
            'DeepArchiveSize': {
                'location': 'xml',
                'sentAs': 'DeepArchiveSize',
                'type': 'number'
            },
            'DeepArchiveObjectNumber': {
                'location': 'xml',
                'sentAs': 'DeepArchiveObjectNumber',
                'type': 'number'
            },
            'HighPerformanceSize': {
                'location': 'xml',
                'sentAs': 'HighPerformanceSize',
                'type': 'number'
            },
            'HighPerformanceObjectNumber': {
                'location': 'xml',
                'sentAs': 'HighPerformanceObjectNumber',
                'type': 'number'
            },
        }
    })
};

export const GetBucketStoragePolicy = {
    'httpMethod': 'GET',
    'urlPath': 'storageClass',
    'parameters': {
        'Bucket': {
            'required': true,
            'location': 'uri',
        },
    }
}

export const setBucketStoragePolicy = (isObs) => {
    if (isObs) {
        return ({
            'httpMethod': 'PUT',
            'urlPath': 'storageClass',
            'parameters': {
                'Bucket': {
                    'required': true,
                    'location': 'uri',
                },
                'StorageClass': {
                    'required': true,
                    'location': 'xml',
                    'type': 'adapter',
                    'sentAs': 'StorageClass'
                }
            }
        })
    }
    return ({
        'httpMethod': 'PUT',
        'urlPath': 'storagePolicy',
        'data': {
            'xmlRoot': 'StoragePolicy',
        },
        'parameters': {
            'Bucket': {
                'required': true,
                'location': 'uri',
            },
            'StorageClass': {
                'required': true,
                'location': 'xml',
                'type': 'adapter',
                'sentAs': 'DefaultStorageClass'
            }
        }
    })
}


export const GetBucketStoragePolicyOutput = (isObs) => {
    if (isObs) {
        return ({
            'data': {
                'type': 'xml',
            },
            'parameters': {
                'StorageClass': {
                    'location': 'xml',
                    'type': 'string',
                    'sentAs': 'StorageClass'
                }
            }
        })
    }
    return ({
        'data': {
            'type': 'xml',
            'xmlRoot': 'StoragePolicy',
        },
        'parameters': {
            'StorageClass': {
                'location': 'xml',
                'type': 'string',
                'sentAs': 'DefaultStorageClass'
            }
        }
    })
}
