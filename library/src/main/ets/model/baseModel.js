/**
 * Copyright 2019 Huawei Technologies Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 */
export const getStorageClassHeader = isObs => isObs ? "x-obs-storage-class" : "x-default-storage-class";
export const getLocationHeader = isObs => isObs ? "bucket-location" : "bucket-region";
export const getLocationXML = isObs => isObs ? "Location" : "LocationConstraint"

const obsGrantee = {
	'type': 'object',
	'sentAs': 'Grantee',
	'parameters': {
		'Type': {
			'type': 'ignore',
		},
		'ID': {
			'sentAs': 'ID',
		},
		'URI': {
			'sentAs': 'Canned',
			'type': 'adapter'
		}
	},
}

const v2Grantee = {
	'data': {
		'xsiNamespace': 'http://www.w3.org/2001/XMLSchema-instance',
		'xsiType': 'Type',
	},
	'type': 'object',
	'sentAs': 'Grantee',
	'parameters': {
		'Type': {
			'type': 'ignore',
		},
		'ID': {
			'sentAs': 'ID',
			'notAllowEmpty': true,
		},
		'Name': {
			'sentAs': 'DisplayName',
		},
		'URI': {
			'sentAs': 'URI',
			'type': 'adapter',
			'notAllowEmpty': true,
		}
	},
}

export const initiator = {
	'type': 'object',
	'location': 'xml',
	'sentAs': 'Initiator',
	'parameters': {
		'ID': {
			'sentAs': 'ID',
		},
		'Name': {
			'sentAs': 'DisplayName',
		}
	},
};

export const grants = isObs => ({
	'type': 'array',
	'location': 'xml',
	'wrapper': 'AccessControlList',
	'sentAs': 'Grant',
	'items': {
		'type': 'object',
		'parameters': {
			'Grantee': isObs ? obsGrantee : v2Grantee,
			'Permission': {
				'sentAs': 'Permission',
			},
			'Delivered': {
				'sentAs': 'Delivered',
			}
		},
	},
});

export const loggingEnabled = isObs => ({
	'type': 'object',
	'location': 'xml',
	'sentAs': 'LoggingEnabled',
	'parameters': {
		'TargetBucket': {
			'sentAs': 'TargetBucket',
		},
		'TargetPrefix': {
			'sentAs': 'TargetPrefix',
		},
		'TargetGrants': {
			'type': 'array',
			'wrapper': 'TargetGrants',
			'sentAs': 'Grant',
			'items': {
				'type': 'object',
				'parameters': {
					'Grantee': isObs ? obsGrantee : v2Grantee,
					'Permission': {
						'sentAs': 'Permission',
					},
				},
			},
		},
	},
});

export const bucketEncryptionRule = {
	'type': 'object',
	'location': 'xml',
	'sentAs': 'Rule',
	'parameters': {
		'ApplyServerSideEncryptionByDefault': {
			'type': 'object',
			'sentAs': 'ApplyServerSideEncryptionByDefault',
			'parameters': {
				'SSEAlgorithm': {
					'sentAs': 'SSEAlgorithm'
				},
				'KMSMasterKeyID': {
					'sentAs': 'KMSMasterKeyID'
				},
				'ProjectID': {
					'sentAs': 'ProjectID'
				},
				'KMSDataEncryption': {
					'sentAs': 'KMSDataEncryption'
				}
			}
		}
	}
};

const filter = {
	'type': 'object',
	'parameters': {
		'FilterRules': {
			'wrapper': 'Object',
			'type': 'array',
			'sentAs': 'FilterRule',
			'items': {
				'type': 'object',
				'parameters': {
					'Name': {},
					'Value': {}
				}
			}
		}
	}
}

export const functionGraphConfiguration = {
	'type': 'array',
	'location': 'xml',
	'sentAs': 'FunctionGraphConfiguration',
	'items': {
		'type': 'object',
		'location': 'xml',
		'parameters': {
			'ID': {
				'sentAs': 'Id'
			},
			'Filter': filter,
			'FunctionGraph': {},

			'Event': {
				'type': 'array',
				'items': {
					'type': 'adapter',
				}
			}
		}
	}
};

export const topicConfiguration = {
	'type': 'array',
	'location': 'xml',
	'sentAs': 'TopicConfiguration',
	'items': {
		'type': 'object',
		'location': 'xml',
		'parameters': {
			'ID': {
				'sentAs': 'Id'
			},
			'Filter': filter,
			'Topic': {},

			'Event': {
				'type': 'array',
				'items': {
					'type': 'adapter'
				}
			}
		}
	}
};

export const eventGridConfiguration = {
	'type': 'array',
	'location': 'xml',
	'sentAs': 'EventGridConfiguration',
	'items': {
		'type': 'object',
		'location': 'xml',
		'parameters': {
			'ID': {
				'sentAs': 'Id'
			},
			'Filter': filter,
			'ChannelId': {},
			'ObjectKeyEncode': {},
			'UseOBSMessageLayout': {},
			'Event': {
				'type': 'array',
				'items': {
					'type': 'adapter'
				}
			}
		}
	}
};

export const functionStageConfiguration = {
	'type': 'array',
	'location': 'xml',
	'sentAs': 'FunctionStageConfiguration',
	'items': {
		'type': 'object',
		'location': 'xml',
		'parameters': {
			'ID': {
				'sentAs': 'Id'
			},
			'Filter': filter,
			'FunctionStage': {},

			'Event': {
				'type': 'array',
				'items': {
					'type': 'adapter',
				}
			}
		}
	}
};

export const inventoryConfiguration = {
	'type': 'object',
	'location': 'xml',
	'sentAs': 'InventoryConfiguration',
	'parameters': {
		'Id': {
			'sentAs': 'Id',
		},
		'IsEnabled': {
			'sentAs': 'IsEnabled',

		},
		'Filter': {
			'type': 'object',
			'sentAs': 'Filter',
			'parameters': {
				'Prefix': {
					'sentAs': 'Prefix'
				}
			}
		},
		'Destination': {
			'type': 'object',
			'sentAs': 'Destination',
			'parameters': {
				'Format': {
					'sentAs': 'Format'
				},
				'Bucket': {
					'sentAs': 'Bucket'
				},
				'Prefix': {
					'sentAs': 'Prefix'
				},
				'Encryption': {
					'type': 'object',
					'sentAs': 'Encryption',
					'parameters': {
						'SSE-KMS': {
							'type': 'object',
							'sentAs': 'SSE-KMS',
							'parameters': {
								'KeyId': {
									'sentAs': 'KeyId',
									'type': 'adapter'
								}
							}
						}
					}
				},
			}
		},
		'Schedule': {
			'type': 'object',
			'sentAs': 'Schedule',
			'parameters': {
				'Frequency': {
					'sentAs': 'Frequency'
				}
			}
		},
		'IncludedObjectVersions': {
			'sentAs': 'IncludedObjectVersions'
		},
		'OptionalFields': {
			'type': 'object',
			'location': 'xml',
			'sentAs': 'OptionalFields',
			'parameters': {
				'Field': {
					'type': 'array',
					'sentAs': 'Field',
					'items': {
						'type': 'string'
					}
				}
			}
		}
	}
};

export const routingRules = {
	'type': 'array',
	'wrapper': 'RoutingRules',
	'location': 'xml',
	'sentAs': 'RoutingRule',
	'items': {
		'type': 'object',
		'parameters': {
			'Condition': {
				'type': 'object',
				'sentAs': 'Condition',
				'parameters': {
					'HttpErrorCodeReturnedEquals': {
						'sentAs': 'HttpErrorCodeReturnedEquals',
					},
					'KeyPrefixEquals': {
						'sentAs': 'KeyPrefixEquals',
					},
				},
			},
			'Redirect': {
				'type': 'object',
				'sentAs': 'Redirect',
				'parameters': {
					'HostName': {
						'sentAs': 'HostName',
					},
					'HttpRedirectCode': {
						'sentAs': 'HttpRedirectCode',
					},
					'Protocol': {
						'sentAs': 'Protocol',
					},
					'ReplaceKeyPrefixWith': {
						'sentAs': 'ReplaceKeyPrefixWith',
					},
					'ReplaceKeyWith': {
						'sentAs': 'ReplaceKeyWith',
					}
				}
			},
		},
	},
};

export const indexDocument = {
	'type': 'object',
	'location': 'xml',
	'sentAs': 'IndexDocument',
	'parameters': {
		'Suffix': {
			'sentAs': 'Suffix',
		},
	}
};

export const errorDocument = {
	'type': 'object',
	'location': 'xml',
	'sentAs': 'ErrorDocument',
	'parameters': {
		'Key': {
			'sentAs': 'Key',
		},
	}
};

export const redirectAllRequestsTo = {
	'type': 'object',
	'location': 'xml',
	'sentAs': 'RedirectAllRequestsTo',
	'parameters': {
		'HostName': {
			'sentAs': 'HostName',
		},
		'Protocol': {
			'sentAs': 'Protocol',
		},
	}
};

export const corsRule = {
	'required': true,
	'type': 'array',
	'location': 'xml',
	'sentAs': 'CORSRule',
	'items': {
		'type': 'object',
		'parameters': {
			'ID': {
				'sentAs': 'ID',
			},
			'AllowedMethod': {
				'type': 'array',
				'sentAs': 'AllowedMethod',
				'items': {
					'type': 'string',
				},
			},
			'AllowedOrigin': {
				'type': 'array',
				'sentAs': 'AllowedOrigin',
				'items': {
					'type': 'string',
				},
			},
			'AllowedHeader': {
				'type': 'array',
				'sentAs': 'AllowedHeader',
				'items': {
					'type': 'string',
				},
			},
			'MaxAgeSeconds': {
				'type': 'number',
				'sentAs': 'MaxAgeSeconds',
			},
			'ExposeHeader': {
				'type': 'array',
				'sentAs': 'ExposeHeader',
				'items': {
					'type': 'string',
				},
			},
		},
	},
};

export const replicationRules = {
	'required': true,
	'type': 'array',
	'location': 'xml',
	'sentAs': 'Rule',
	'items': {
		'type': 'object',
		'parameters': {
			'ID': {
				'sentAs': 'ID',
			},
			'Prefix': {
				'sentAs': 'Prefix',
			},
			'Status': {
				'sentAs': 'Status',
			},
			'Destination': {
				'type': 'object',
				'sentAs': 'Destination',
				'parameters': {
					'Bucket': {
						'sentAs': 'Bucket',
						'type': 'adapter'
					},
					'StorageClass': {
						'sentAs': 'StorageClass',
						'type': 'adapter'
					},
					'DeleteData': {
						'sentAs': 'DeleteData',
						'type': 'string'
					}
				}
			},
			'HistoricalObjectReplication': {
				'sentAs': 'HistoricalObjectReplication'
			}
		},
	}
};

export const owner = {
	'type': 'object',
	'location': 'xml',
	'sentAs': 'Owner',
	'parameters': {
		'ID': {
			'sentAs': 'ID'
		},
		'Name': {
			'sentAs': 'DisplayName',
		}
	}
};

export const commonPrefixes = {
	'type': 'array',
	'location': 'xml',
	'sentAs': 'CommonPrefixes',
	'items': {
		'type': 'object',
		'parameters': {
			'Prefix': {
				'decode': true,
				'sentAs': 'Prefix',
			},
			'MTime': {
				'sentAs': 'MTime',
			},
			'InodeNo': {
				'sentAs': 'InodeNo',
			}
		}
	}
};

export const customDomainBody = {
	'Name': {
		'location': 'xml',
		'sentAs': 'Name',
	},
	'CertificateId': {
		'location': 'xml',
		'sentAs': 'CertificateId',
	},
	'Certificate': {
		'location': 'xml',
		'sentAs': 'Certificate',
	},
	'CertificateChain': {
		'location': 'xml',
		'sentAs': 'CertificateChain',
	},
	'PrivateKey': {
		'location': 'xml',
		'sentAs': 'PrivateKey',
	},
};

export const CDNNotifyConfiguration = {
	'location': 'xml',
	'sentAs': 'Domain',
	'type': 'array',
	'items': {
		'type': 'object',
		'parameters': {
			'Name': {
				'type': 'string',
				'sentAs': 'Name'
			},
			'Action': {
				'type': 'string',
				'sentAs': 'Action'
			},
			'Status': {
				'type': 'string',
				'sentAs': 'Status'
			},
			'Agency': {
				'type': 'string',
				'sentAs': 'Agency'
			},
			'Filter': {
				'type': 'object',
				'sentAs': 'Filter',
				'parameters': {
					'Object': {
						'type': 'object',
						'sentAs': 'Object',
						'parameters': {
							'FilterRule': {
								'type': 'array',
								'sentAs': 'FilterRule',
								'items': {
									'type': 'object',
									'parameters': {
										'Name': {
											'type': 'string',
											'sentAs': 'Name'
										},
										'Value': {
											'type': 'string',
											'sentAs': 'Value'
										},
									}
								}
							}
						}
					}

				}
			},
			'Events': {
				'type': 'array',
				'items': {
					'type': 'adapter',
				},
				'sentAs': 'Event'
			}
		}
	}
};

export const lifecycleRule = {
	'required': true,
	'type': 'array',
	'location': 'xml',
	'sentAs': 'Rule',
	'items': {
		'type': 'object',
		'parameters': {
			'ID': {
				'sentAs': 'ID',
			},
			'Prefix': {
				'sentAs': 'Prefix',
			},
			'Status': {
				'sentAs': 'Status',
			},
			'Filter': {
				'type': 'object',
				'sentAs': 'Filter',
				'parameters': {
					// 即使array中仅有一条记录也可以使用And属性
					'And': {
						'type': 'object',
						'sentAs': 'And',
						'parameters': {
							'Prefix': {
								'sentAs': 'Prefix'
							},
							'Tag': {
								'type': 'array',
								'sentAs': 'Tag',
								'items': {
									'type': 'object',
									'location': 'xml',
									'sentAs': 'Tag',
									'parameters': {
										'Key': {
											'sentAs': 'Key'
										},
										'Value': {
											'sentAs': 'Value'
										}
									}
								}
							},
						}
					}
				}
			},
			'Transitions': {
				'type': 'array',
				'sentAs': 'Transition',
				'items': {
					'type': 'object',
					'parameters': {
						'StorageClass': {
							'sentAs': 'StorageClass',
							'type': 'adapter'
						},
						'Date': {
							'sentAs': 'Date',
						},
						'Days': {
							'type': 'number',
							'sentAs': 'Days'
						}
					}
				}
			},
			'Expiration': {
				'type': 'object',
				'sentAs': 'Expiration',
				'parameters': {
					'Date': {
						'sentAs': 'Date',
					},
					'Days': {
						'type': 'number',
						'sentAs': 'Days'
					},
					'ExpiredObjectDeleteMarker': {
						'sentAs': 'ExpiredObjectDeleteMarker'
					}
				},
			},
			'AbortIncompleteMultipartUpload': {
				'type': 'object',
				'sentAs': 'AbortIncompleteMultipartUpload',
				'parameters': {
					'DaysAfterInitiation': {
						'type': 'number',
						'sentAs': 'DaysAfterInitiation',
					},
				},
			},
			'NoncurrentVersionTransitions': {
				'type': 'array',
				'sentAs': 'NoncurrentVersionTransition',
				'items': {
					'type': 'object',
					'parameters': {
						'StorageClass': {
							'sentAs': 'StorageClass',
							'type': 'adapter'
						},
						'NoncurrentDays': {
							'type': 'number',
							'sentAs': 'NoncurrentDays'
						}
					}
				}
			},
			'NoncurrentVersionExpiration': {
				'type': 'object',
				'sentAs': 'NoncurrentVersionExpiration',
				'parameters': {
					'NoncurrentDays': {
						'type': 'number',
						'sentAs': 'NoncurrentDays',
					},
				},
			}
		},
	},
};

export const backToSourceRules = {
	'sentAs': 'BackToSourceRule',
	'required': true,
	'location': 'xml',
	'type': "array",
	'items': {
		'type': "object",
		'parameters': {
			'ID': {
				'sentAs': "ID"
			},
			'Condition': {
				'sentAs': "Condition",
				'type': "object",
				'parameters': {
					'ObjectKeyPrefixEquals': {
						'sentAs': 'ObjectKeyPrefixEquals'
					},
					'HttpErrorCodeReturnedEquals': {
						'sentAs': 'HttpErrorCodeReturnedEquals'
					}
				},
			},
			'Redirect': {
				'sentAs': "Redirect",
				'type': "object",
				'parameters': {
					'HttpRedirectCode': {
						'sentAs': 'HttpRedirectCode'
					},
					'SourceEndpoint': {
						'sentAs': 'SourceEndpoint'
					},
					'SourceBucketName': {
						'sentAs': 'SourceBucketName'
					},
					'ReplaceKeyWith': {
						'sentAs': 'ReplaceKeyWith'
					},
					'StaticUri': {
						'sentAs': 'StaticUri'
					},
					'ReplaceKeyPrefixWith': {
						'sentAs': 'ReplaceKeyPrefixWith'
					},
					'MigrationConfiguration': {
						'sentAs': 'MigrationConfiguration',
						'type': 'object',
						'parameters': {
							'Agency': {
								'sentAs': 'Agency',
							},
							'LogBucketName': {
								'sentAs': 'LogBucketName',
							},
							'PrivateBucketConfiguration': {
								'sentAs': 'PrivateBucketConfiguration',
								'type': 'object',
								'parameters': {
									'SourceStorageProvider': {
										'sentAs': 'SourceStorageProvider'
									},
									'SourceBucketAK': {
										'sentAs': 'SourceBucketAK'
									},
									'SourceBucketSK': {
										'sentAs': 'SourceBucketSK'
									},
									'SourceBucketZone': {
										'sentAs': 'SourceBucketZone'
									}
								}
							}
						}
					}
				}
			}
		}
	}
};

export const objectLock = {
	'location': 'xml',
	'type': 'object',
	'parameters': {
		'DefaultRetention': {
			'type': 'object',
			'sentAs': 'DefaultRetention',
			'parameters': {
				'Days': {
					'sentAs': 'Days'
				},
				'Years': {
					'sentAs': 'Years'
				},
				'Mode': {
					'sentAs': 'Mode'
				}
			}
		}

	}
};

export const objectEncryptionRule = {
	'SseKms': {
		'location': 'header',
		'sentAs': 'server-side-encryption',
		'withPrefix': true,
		'type': 'adapter'
	},
	'SseMode': {
		'location': 'header',
		'sentAs': 'server-side-encryption',
		'withPrefix': true,
		'type': 'adapter'
	},
	'SseAlgorithm': {
		'location': 'header',
		'sentAs': 'server-side-data-encryption',
		'withPrefix': true,
	},
	'SseKmsKey': {
		'location': 'header',
		'sentAs': 'server-side-encryption-kms-key-id',
		'withPrefix': true,
	},
	'SseKmsProjectId': {
		'location': 'header',
		'sentAs': 'sse-kms-key-project-id',
		'withPrefix': true,
	},
	'SseC': {
		'location': 'header',
		'sentAs': 'server-side-encryption-customer-algorithm',
		'withPrefix': true,
	},
	'SseCKey': {
		'location': 'header',
		'sentAs': 'server-side-encryption-customer-key',
		'type': 'password',
		'withPrefix': true,
	},
	'SseCKeyMd5': {
		'location': 'header',
		'sentAs': 'server-side-encryption-customer-key-MD5',
		'withPrefix': true
	}
}

export const tagSet = {
	'required': true,
	'type': 'array',
	'location': 'xml',
	'wrapper': 'TagSet',
	'sentAs': 'Tag',
	'items': {
		'type': 'object',
		'parameters': {
			'Key': {
				'sentAs': 'Key',
			},
			'Value': {
				'sentAs': 'Value',
			}
		}
	}
};

export const templates = {
	'ApiPath': {
		'location': 'uri'
	},
	'OtherParameter': {
		'location': 'uri',
		'sentAs': 'template_name_prefix'
	},
	'XObsCategory': {
		'location': 'urlPath',
		'sentAs': 'x-workflow-category'
	},
	'XObsOtatus': {
		'location': 'urlPath',
		'sentAs': 'x-workflow-status',
	},
	'XObsPrefix': {
		'location': 'urlPath',
		'sentAs': 'x-workflow-prefix'
	}
}
export const baseOptionInput = {
	'Origin': {
		'required': true,
		'location': 'header',
		'sentAs': 'Origin',
	},
	'AccessControlRequestMethods': {
		'required': true,
		'type': 'array',
		'location': 'header',
		'sentAs': 'Access-Control-Request-Method',
		'items': {
			'type': 'string',
		},
	},
	'AccessControlRequestHeaders': {
		'type': 'array',
		'location': 'header',
		'sentAs': 'Access-Control-Request-Headers',
		'items': {
			'type': 'string',
		},
	},
}

export const optionOutput = {
	'AllowOrigin': {
		'location': 'header',
		'sentAs': 'access-control-allow-origin',
	},
	'AllowHeader': {
		'location': 'header',
		'sentAs': 'access-control-allow-headers',
	},
	'AllowMethod': {
		'location': 'header',
		'sentAs': 'access-control-allow-methods',
	},
	'ExposeHeader': {
		'location': 'header',
		'sentAs': 'access-control-expose-headers',
	},
	'MaxAgeSeconds': {
		'location': 'header',
		'sentAs': 'access-control-max-age',
		'type': 'number',
	},
}