/**
 * Copyright 2019 Huawei Technologies Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 */

import {
  createBucket,
  DeleteBucketDirectColdAccess,
  DeleteBucketDirectColdAccessOutput,
  DeleteBucketInventory,
  DeleteBucketInventoryOutput,
  DeleteBucketLifecycleConfiguration,
  DeleteBucketMirrorBackToSource,
  DeleteBucketPolicy,
  DeleteBucketWebsiteConfiguration,
  GetBucketAcl,
  getBucketAclOutput,
  GetBucketDirectColdAccess,
  GetBucketDirectColdAccessOutput,
  GetBucketInventory,
  GetBucketInventoryOutput,
  GetBucketLoggingConfiguration,
  getBucketLoggingConfigurationOutput,
  GetBucketMirrorBackToSource,
  GetBucketMirrorBackToSourceOutput,
  GetBucketPolicy,
  GetBucketPolicyOutput,
  GetBucketQuota,
  GetBucketQuotaOutput,
  GetBucketRequesterPay,
  GetBucketRequesterPayOutput,
  GetBucketStorageInfo,
  getBucketStorageInfoOutput,
  GetBucketVersioningConfiguration,
  GetBucketVersioningConfigurationOutput,
  GetBucketWebsiteConfiguration,
  GetBucketWebsiteConfigurationOutput,
  HeadApiVersion,
  HeadApiVersionOutput,
  HeadBucket,
  ListObjects,
  listObjectsOutput,
  ListVersions,
  listVersionsOutput,
  setBucketAcl,
  SetBucketDirectColdAccess,
  SetBucketDirectColdAccessOutput,
  SetBucketEncryptionOutput,
  SetBucketInventory,
  SetBucketInventoryOutput,
  SetBucketLifecycleConfiguration,
  SetBucketMirrorBackToSource,
  SetBucketMirrorBackToSourceOutput,
  SetBucketNotification,
  SetBucketPolicy,
  SetBucketQuota,
  SetBucketRequesterPay,
  SetBucketRequesterPayOutput,
  SetBucketVersioningConfiguration,
  SetBucketWebsiteConfiguration,
  DeleteBucketCustomDomain,
  DeleteBucketEncryption,
  DeleteBucketEncryptionOutput,
  DeleteBucketReplication,
  DeleteBucketTagging,
  GetBucketCors,
  GetBucketCorsOutput,
  GetBucketCustomDomain,
  GetBucketCustomDomainOutput,
  GetBucketEncryption,
  GetBucketEncryptionOutput,
  GetBucketLifecycleConfiguration,
  GetBucketLifecycleConfigurationOutput,
  GetBucketNotification,
  GetBucketNotificationOutput,
  GetBucketReplication,
  GetBucketReplicationOutput,
  GetBucketTagging,
  GetBucketTaggingOutput,
  OptionsBucket,
  OptionsBucketOutput,
  SetBucketCors,
  SetBucketCustomDomain,
  SetBucketEncryption,
  SetBucketReplication,
  SetBucketTagging,
  GetBucketStoragePolicy,
  DeleteBucketCors,
  setBucketStoragePolicy,
  getBucketLocationOutput,
  GetBucketLocation,
  ListBucketsOutput,
  ListBuckets,
  DeleteBucket,
  GetBucketMetadata,
  getBucketMetadataOutput,
  GetBucketStoragePolicyOutput,
  setBucketLoggingConfiguration,
} from "./model/bucketModel";

import {
  AppendObject,
  AppendObjectOutput,
  CopyObject,
  CopyObjectOutput,
  DeleteObject,
  DeleteObjectOutput,
  DeleteObjects,
  DeleteObjectsOutput,
  DeleteObjectTagging,
  GetObject,
  GetObjectAcl,
  getObjectAclOutput,
  GetObjectMetadata,
  GetObjectMetadataOutput,
  GetObjectOutput,
  GetObjectTagging,
  GetObjectTaggingOutput,
  ModifyObject,
  ModifyObjectOutput,
  PutObject,
  PutObjectOutput,
  RenameObject,
  RenameObjectOutput,
  restoreObject,
  setObjectAcl,
  SetObjectAclOutput,
  SetObjectMetadata,
  SetObjectMetadataOutput,
  SetObjectTagging,
  TruncateObject,
} from "./model/objectModel";

import {
  createWorkflowTrigger,
  deleteWorkflowTrigger,
  GetCDNNotifyConfiguration,
  GetCDNNotifyConfigurationOutput,
  GetQuota,
  GetQuotaOutput,
  getWorkflowTrigger,
  GetWorkflowTriggerOutput,
  CreateTemplate,
  CreateWorkflow,
  RestoreFailedWorkflowExecution,
  CreateAuditPolicy,
  CreateAuditPolicyOutput,
  GetAuditResult,
  GetAuditResultOutput,
  DeleteAuditPolicy,
  PutAuditPolicyOutput,
  PutAuditPolicy,
  GetAuditPolicyOutput,
  GetAuditPolicy,
  DeleteWorkflow,
  UpdateWorkflow,
  GetWorkflowList,
  GetWorkflowListOutput,
  GetWorkflowTemplateList,
  GetWorkflowTemplateListOutput,
  GetWorkflowInstanceList,
  GetWorkflowInstanceListOutput,
  SetCDNNotifyConfiguration,
  DeleteBackToSource,
  SetBucketCdnNotifyConfiguration,
  GetBucketCDNNotifyConfiguration,
  GetBucketCDNNotifyConfigurationOutput,
  SetBackToSource,
  DeleteBucketDisPolicy,
  GetBucketDisPolicy,
  GetBucketDisPolicyOutput,
  SetBucketDisPolicy,
  BindBucketAlias,
  BindBucketAliasOutput,
  DeleteBucketAlias,
  GetBackToSource,
  GetBackToSourceOutput,
  GetBucketAlias,
  GetBucketAliasOutput,
  GetBucketObjectLockConfiguration,
  GetBucketObjectLockConfigurationOutput,
  ListBucketsAlias,
  ListBucketsAliasOutput,
  SetBucketAlias,
  SetBucketObjectLockConfig,
  UnbindBucketAlias,
} from "./model/otherModel";

import {
  AbortMultipartUpload,
  CompleteMultipartUpload,
  CompleteMultipartUploadOutput,
  CopyPart,
  CopyPartOutput,
  InitiateMultipartUpload,
  InitiateMultipartUploadOutput,
  ListMultipartUploads,
  ListMultipartUploadsOutput,
  ListParts,
  ListPartsOutput,
  UploadPart,
  UploadPartOutput,
} from "./model/partModel";

import {
  createSfsBucket,
  DeleteSFSAcl,
  DeleteSfsBucket,
  DeleteSFSPermissionAcl,
  DeleteSFSPermissionGroup,
  GetSFSAcl,
  GetSFSAclOutput,
  GetSFSPermissionAcl,
  GetSFSPermissionAclOutput,
  GetSFSPermissionGroup,
  GetSFSPermissionGroupList,
  GetSFSPermissionGroupListOutput,
  GetSFSPermissionGroupOutput,
  SetSFSAcl,
  SetSFSPermissionGroup,
  UpdateSFSPermissionAcl,
  UpdateSFSPermissionGroup,
} from "./model/sfsBucketModel";

const getModel = isObs => ({
  HeadBucket,

  HeadApiVersion,
  HeadApiVersionOutput,

  CreateBucket: createBucket(isObs),

  GetBucketMetadata,
  GetBucketMetadataOutput: getBucketMetadataOutput(isObs),

  DeleteBucket,

  ListBuckets,
  ListBucketsOutput,

  GetBucketLocation,
  GetBucketLocationOutput: getBucketLocationOutput(isObs),

  // check
  ListObjects,
  ListObjectsOutput: listObjectsOutput(isObs),
  ListVersions,
  ListVersionsOutput: listVersionsOutput(isObs),

  SetBucketAcl: setBucketAcl(isObs),
  GetBucketAcl,
  GetBucketAclOutput: getBucketAclOutput(isObs),

  SetBucketVersioningConfiguration,
  GetBucketVersioningConfiguration,
  GetBucketVersioningConfigurationOutput,

  SetBucketEncryption,
  SetBucketEncryptionOutput,
  GetBucketEncryption,
  GetBucketEncryptionOutput,
  DeleteBucketEncryption,
  DeleteBucketEncryptionOutput,

  SetBucketTagging,
  GetBucketTagging,
  GetBucketTaggingOutput,
  DeleteBucketTagging,

  SetBucketReplication,
  GetBucketReplication,
  GetBucketReplicationOutput,
  DeleteBucketReplication,

  SetBucketInventory,
  SetBucketInventoryOutput,
  GetBucketInventory,
  GetBucketInventoryOutput,
  DeleteBucketInventory,
  DeleteBucketInventoryOutput,

  SetBucketCors,
  GetBucketCors,
  GetBucketCorsOutput,
  DeleteBucketCors,

  SetBucketLoggingConfiguration: setBucketLoggingConfiguration(isObs),
  GetBucketLoggingConfiguration,
  GetBucketLoggingConfigurationOutput: getBucketLoggingConfigurationOutput(isObs),

  SetBucketNotification,
  GetBucketNotification,
  GetBucketNotificationOutput,

  SetBucketLifecycleConfiguration,
  GetBucketLifecycleConfiguration,
  GetBucketLifecycleConfigurationOutput,
  DeleteBucketLifecycleConfiguration,

  SetBucketCustomDomain,
  GetBucketCustomDomain,
  GetBucketCustomDomainOutput,
  DeleteBucketCustomDomain,

  GetBucketStorageInfo,
  GetBucketStorageInfoOutput: getBucketStorageInfoOutput(isObs),

  SetBucketQuota,
  GetBucketQuota,
  GetBucketQuotaOutput,

  SetBucketPolicy,
  GetBucketPolicy,
  GetBucketPolicyOutput,
  DeleteBucketPolicy,

  SetBucketDirectColdAccess,
  SetBucketDirectColdAccessOutput,
  GetBucketDirectColdAccess,
  GetBucketDirectColdAccessOutput,
  DeleteBucketDirectColdAccess,
  DeleteBucketDirectColdAccessOutput,

  SetBucketMirrorBackToSource,
  SetBucketMirrorBackToSourceOutput,
  GetBucketMirrorBackToSource,
  GetBucketMirrorBackToSourceOutput,
  DeleteBucketMirrorBackToSource,

  GetBucketRequesterPay,
  GetBucketRequesterPayOutput,
  SetBucketRequesterPay,
  SetBucketRequesterPayOutput,

  SetBucketWebsiteConfiguration,
  GetBucketWebsiteConfiguration,
  GetBucketWebsiteConfigurationOutput,
  DeleteBucketWebsiteConfiguration,

  SetBucketCdnNotifyConfiguration,
  GetBucketCDNNotifyConfiguration,
  GetBucketCDNNotifyConfigurationOutput,

  OptionsBucket,
  OptionsBucketOutput,

  PutBackToSource: SetBackToSource,
  GetBackToSource,
  GetBackToSourceOutput,
  DeleteBackToSource,

  SetBucketStoragePolicy: setBucketStoragePolicy(isObs),
  GetBucketStoragePolicy,
  GetBucketStoragePolicyOutput: GetBucketStoragePolicyOutput(isObs),

  SetBucketAlias,
  GetBucketAlias,
  GetBucketAliasOutput,
  DeleteBucketAlias,
  BindBucketAlias,
  BindBucketAliasOutput,
  UnbindBucketAlias,
  ListBucketsAlias,
  ListBucketsAliasOutput,

  SetBucketDisPolicy,
  GetBucketDisPolicy,
  GetBucketDisPolicyOutput,
  DeleteBucketDisPolicy,

  SetBucketObjectLockConfig,
  GetBucketObjectLockConfiguration,
  GetBucketObjectLockConfigurationOutput,

  CreateSfsBucket: createSfsBucket(isObs),
  DeleteSfsBucket,

  SetSFSAcl,
  GetSFSAcl,
  GetSFSAclOutput,
  DeleteSFSAcl,

  GetSFSPermissionAcl,
  GetSFSPermissionAclOutput,
  UpdateSFSPermissionAcl,
  DeleteSFSPermissionAcl,

  GetSFSPermissionGroupList,
  GetSFSPermissionGroupListOutput,

  SetSFSPermissionGroup,
  UpdateSFSPermissionGroup,
  GetSFSPermissionGroup,
  GetSFSPermissionGroupOutput,
  DeleteSFSPermissionGroup,

  // 对象接口
  PutObject,
  PutObjectOutput,

  SetObjectAcl: setObjectAcl(isObs),
  SetObjectAclOutput,
  GetObjectAcl,
  GetObjectAclOutput: getObjectAclOutput(isObs),

  SetObjectMetadata,
  SetObjectMetadataOutput,
  GetObjectMetadata,
  GetObjectMetadataOutput,

  RestoreObject: restoreObject(isObs),

  CopyObject,
  CopyObjectOutput,

  GetObject,
  GetObjectOutput,

  AppendObject,
  AppendObjectOutput,

  SetObjectTagging,
  GetObjectTagging,
  GetObjectTaggingOutput,
  DeleteObjectTagging,

  DeleteObject,
  DeleteObjectOutput,

  DeleteObjects,
  DeleteObjectsOutput,

  // 并行文件系统接口
  ModifyObject,
  ModifyObjectOutput,

  RenameObject,
  RenameObjectOutput,

  TruncateObject,

  // 段接口
  InitiateMultipartUpload,
  InitiateMultipartUploadOutput,

  ListParts,
  ListPartsOutput,

  ListMultipartUploads,
  ListMultipartUploadsOutput,

  UploadPart,
  UploadPartOutput,

  CopyPart,
  CopyPartOutput,

  CompleteMultipartUpload,
  CompleteMultipartUploadOutput,

  AbortMultipartUpload,

  // 其他接口
  GetQuota,
  GetQuotaOutput,

  SetCDNNotifyConfiguration,
  GetCDNNotifyConfiguration,
  GetCDNNotifyConfigurationOutput,

  CreateWorkflowTrigger: createWorkflowTrigger(isObs),
  GetWorkflowTrigger: getWorkflowTrigger(isObs),
  GetWorkflowTriggerOutput,
  DeleteWorkflowTrigger: deleteWorkflowTrigger(isObs),

  RestoreFailedWorkflowExecution,

  CreateTemplate,
  CreateWorkflow,

  CreateAuditPolicy,
  CreateAuditPolicyOutput,

  GetAuditPolicy,
  GetAuditPolicyOutput,

  PutAuditPolicy,
  PutAuditPolicyOutput,

  DeleteAuditPolicy,

  GetAuditResult,
  GetAuditResultOutput,

  UpdateWorkflow,
  GetWorkflowList,
  GetWorkflowListOutput,
  DeleteWorkflow,

  GetWorkflowTemplateList,
  GetWorkflowTemplateListOutput,

  GetWorkflowInstanceList,
  GetWorkflowInstanceListOutput,
  
});

export default getModel;
