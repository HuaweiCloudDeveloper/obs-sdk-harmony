export const BucketType = {
    OBJECT: "OBJECT",
    POSIX: "POSIX",
}

export const AclType = {
    PRIVATE: 'private',
    PUBLIC_READ: 'public-read',
    PUBLIC_READ_WRITE: 'public-read-write',
    PUBLIC_READ_DELIVERED: 'public-read-delivered',
    PUBLIC_READ_WRITE_DELIVERED: 'public-read-write-delivered',
    AUTHENTICATED_READ: 'authenticated-read',
    BUCKET_OWNER_READ: 'bucket-owner-read',
    BUCKET_OWNER_FULL_CONTROL: 'bucket-owner-full-control',
    LOG_DELIVERY_WRITE: 'log-delivery-write',
}

export const StorageClassType = {
    STANDARD: 'STANDARD',
    WARM: 'WARM',
    COLD: 'COLD',
    DEEP_ARCHIVE: 'DEEP_ARCHIVE',
    INTELLIGENT_TIERING: 'INTELLIGENT_TIERING',
}

export const PermissionType = {
    READ: 'READ',
    WRITE: 'WRITE',
    READ_ACP: 'READ_ACP',
    WRITE_ACP: 'WRITE_ACP',
    FULL_CONTROL: 'FULL_CONTROL',
}

export const GroupUriType = {
    ALL_USERS: 'AllUsers',
    AUTHENTICATED_USERS: 'AuthenticatedUsers',
    LOG_DELIVERY: 'LogDelivery',
}

export const RestoreTierType = {
    EXPEDITED: 'Expedited',
    STANDARD: 'Standard',
    BULK: 'Bulk',
}

export const GranteeType = {
    GROUP: 'Group',
    CANONICAL_USER: 'CanonicalUser',
}

export const MetadataDirectiveType = {
    COPY: 'COPY',
    REPLACE: 'REPLACE',
    REPLACE_NEW: 'REPLACE_NEW',
}

export const EventType = {
    EventObjectCreatedAll: 'ObjectCreated:*',
    EventObjectCreatedPut: 'ObjectCreated:Put',
    EventObjectCreatedPost: 'ObjectCreated:Post',
    EventObjectCreatedCopy: 'ObjectCreated:Copy',
    EventObjectCreatedCompleteMultipartUpload: 'ObjectCreated:CompleteMultipartUpload',
    EventObjectRemovedAll: 'ObjectRemoved:*',
    EventObjectRemovedDelete: 'ObjectRemoved:Delete',
    EventObjectRemovedDeleteMarkerCreated: 'ObjectRemoved:DeleteMarkerCreated',
}

export const CheckSumAlgorithm = {
    CONTENT_MD5: 'Content-MD5',
    CONTENT_SHA256: 'Content-SHA256',
}

export const VersioningStatus = {
    ENABLED: "Enabled",
    SUSPENDED: "Suspended",
}

export const HttpMethodType = {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    DELETE: "DELETE",
    HEAD: "HEAD",
}

export const SignatureType = {
    OBS: "obs",
    V2: "v2",
    V4: "v4",
}

export const LogLevel = {
    OFF: "off",
    DEBUG: "debug",
    INFO: "info",
    WARN: "warn",
    ERROR: "error",
}

export const SaveByType = {
    TEXT: "string",
    ARRAY_BUFFER: "array_buffer",
    OBJECT: "object",
    FILE: "file",
}

export const IncludedObjectVersionsType = {
    ALL: "All",
    CURRENT: "Current",
}

export const FrequencyType = {
    DAILY: "Daily",
    WEEKLY: "Weekly",
}