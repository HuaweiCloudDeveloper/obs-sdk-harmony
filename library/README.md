English | [简体中文](README_CN.md)

<p align="center">
  <a href="https://www.huaweicloud.com/"><img width="270px" height="90px" src="https://console-static.huaweicloud.com/static/authui/20210202115135/public/custom/images/logo-en.svg"></a>
</p>

<h1 align="center">OBS Harmony SDK</h1>

The OBS Harmony SDK allows you to easily work with Object Storage Service（OBS）。

This document introduces how to obtain and use OBS Harmony SDK.

## Requirements

- You must have Huawei Cloud account as well as the Access Key (AK) and Secret key (SK) of the
  Huawei Cloud account. You can create an Access Key in the Huawei Cloud console. For more information,
  see [My Credentials](https://support.huaweicloud.com/en-us/usermanual-ca/en-us_topic_0046606340.html).
- OBS Harmony SDK  requires **Harmony API 9** or later.

## How to Install

``` bash
ohpm install @obs/esdk-obs-harmony
```

For details about the OpenHarmony ohpm environment configuration, see [Installing the OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md).

## Required Permissions
```
ohos.permission.INTERNET
```

## Code Example

```typescript
import ObsClient from '@obs/esdk-obs-harmony';

const obsClient: ObsClient = new ObsClient({
  AccessKeyId: process.env.HUAWEICLOUD_SDK_AK,
  SecretAccessKey: process.env.HUAWEICLOUD_SDK_SK,
  Server: this.endpoint
})

(async () => {
  try {
    const result = await obsClient.listBucket()
    console.log("Result:", JSON.stringify(result, null, 2));
  } catch (error:any) {
    console.error("Exception:", JSON.stringify(error, null, 2));
  }
})();
```

## Constraints

SDK has been verified in the following versions:

DevEco Studio: DevEco Studio NEXT Developer Beta1(5.0.1.100), SDK: API12(Developer preview2)

## How to Contribute

If you find any problem when, submit an [Issue](https://gitcode.com/HuaweiCloudDeveloper/obs-sdk-harmony/issues) or a [PR](https://gitcode.com/HuaweiCloudDeveloper/obs-sdk-harmony/merge_requests) to us.

## License

The repository is based on [MIT](./LICENSE).
