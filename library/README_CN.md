[English](./README.md) | 简体中文

<p align="center">
  <a href="https://www.huaweicloud.com/"><img width="270px" height="90px" src="https://console-static.huaweicloud.com/static/authui/20210202115135/public/custom/images/logo.svg"></a>
</p>

<h1 align="center">OBS Harmony SDK</h1>

OBS Harmony SDK 让您无需关心请求细节即可快速使用对象存储服务（OBS）。

这里将向您介绍如何获取并使用 OBS Harmony SDK 。

## 使用前提

- 您需要拥有云账号以及该账号对应的 Access Key（AK）和 Secret Access Key（SK）。请在华为云控制台“我的凭证-访问密钥”页面上创建和查看您的 AK&SK
  。更多信息请查看 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。
- OBS Harmony SDK 支持鸿蒙 API 9 及以上版本。

## 下载安装

``` bash
ohpm install @obs/esdk-obs-harmony
```

OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 需要权限
```
ohos.permission.INTERNET
```

## 代码示例

```typescript
import ObsClient from '@obs/esdk-obs-harmony';

const obsClient: ObsClient = new ObsClient({
  AccessKeyId: process.env.HUAWEICLOUD_SDK_AK,
  SecretAccessKey: process.env.HUAWEICLOUD_SDK_SK,
  Server: this.endpoint
})

(async () => {
  try {
    const result = await obsClient.listBucket()
    console.log("Result:", JSON.stringify(result, null, 2));
  } catch (error:any) {
    console.error("Exception:", JSON.stringify(error, null, 2));
  }
})();
```

## 约束与限制

在下述版本验证通过：
DevEco Studio: DevEco Studio NEXT Developer Beta1(5.0.1.100), SDK: API12(Developer preview2)


## 贡献代码

使用过程中发现任何问题都可以提[Issue](https://gitcode.com/HuaweiCloudDeveloper/obs-sdk-harmony/issues)给我们，当然，我们也非常欢迎你给我们提[PR](https://gitcode.com/HuaweiCloudDeveloper/obs-sdk-harmony/merge_requests) 。

## 开源协议

本项目基于 [MIT](./LICENSE) ，请自由地享受和参与开源。
