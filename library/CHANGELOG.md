# ChangeLog

## 3.24.9

1. Supports OBS Basic API.
2. Fixed some bugs.

## 3.24.8-alpha.1

1. Supports OBS Basic API.