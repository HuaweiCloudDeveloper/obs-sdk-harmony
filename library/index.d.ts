/**
 * Copyright 2019 Huawei Technologies Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use
 * this file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 *
 */

export enum BucketType {
  OBJECT = "OBJECT",
  POSIX = "POSIX",
}

export enum AclType {
  PRIVATE = "private",
  PUBLIC_READ = "public-read",
  PUBLIC_READ_WRITE = "public-read-write",
  PUBLIC_READ_DELIVERED = "public-read-delivered",
  PUBLIC_READ_WRITE_DELIVERED = "public-read-write-delivered",
  AUTHENTICATED_READ = "authenticated-read",
  BUCKET_OWNER_READ = "bucket-owner-read",
  BUCKET_OWNER_FULL_CONTROL = "bucket-owner-full-control",
  LOG_DELIVERY_WRITE = "log-delivery-write",
}

export enum StorageClassType {
  STANDARD = "STANDARD",
  WARM = "WARM",
  COLD = "COLD",
  DEEP_ARCHIVE = "DEEP_ARCHIVE",
  INTELLIGENT_TIERING = "INTELLIGENT_TIERING",
}

export enum PermissionType {
  READ = "READ",
  WRITE = "WRITE",
  READ_ACP = "READ_ACP",
  WRITE_ACP = "WRITE_ACP",
  FULL_CONTROL = "FULL_CONTROL",
}

export enum GroupUriType {
  ALL_USERS = "AllUsers",
  AUTHENTICATED_USERS = "AuthenticatedUsers",
  LOG_DELIVERY = "LogDelivery",
}

export enum RestoreTierType {
  EXPEDITED = "Expedited",
  STANDARD = "Standard",
  BULK = "Bulk",
}

export enum GranteeType {
  GROUP = "Group",
  CANONICAL_USER = "CanonicalUser",
}

export enum MetadataDirectiveType {
  COPY = "COPY",
  REPLACE = "REPLACE",
  REPLACE_NEW = "REPLACE_NEW",
}

export enum EventType {
  EventObjectCreatedAll = "ObjectCreated:*",
  EventObjectCreatedPut = "ObjectCreated:Put",
  EventObjectCreatedPost = "ObjectCreated:Post",
  EventObjectCreatedCopy = "ObjectCreated:Copy",
  EventObjectCreatedCompleteMultipartUpload = "ObjectCreated:CompleteMultipartUpload",
  EventObjectRemovedAll = "ObjectRemoved:*",
  EventObjectRemovedDelete = "ObjectRemoved:Delete",
  EventObjectRemovedDeleteMarkerCreated = "ObjectRemoved:DeleteMarkerCreated",
}

export enum CheckSumAlgorithm {
  CONTENT_MD5 = "Content-MD5",
  CONTENT_SHA256 = "Content-SHA256",
}

export enum VersioningStatus {
  ENABLED = "Enabled",
  SUSPENDED = "Suspended",
}

export enum FrequencyType {
  DAILY = "Daily",
  WEEKLY = "Weekly",
}

export enum IncludedObjectVersionsType {
  ALL = "All",
  CURRENT = "Current",
}

export enum LogLevel {
  OFF = "off",
  DEBUG = "debug",
  INFO = "info",
  WARN = "warn",
  ERROR = "error",
}

export interface LogConfig {
  level: LogLevel;
}

export interface ObsClientConfig {
  AccessKeyId: string;
  SecretAccessKey: string;
  SecurityToken?: string;
  Server: string;
  IsCname?: string;
  Signature?: SignatureType;
  IsSignatureNegotiation?: boolean;
  PathStyle?: boolean;
  IsSecure?: boolean;
  Port?: number;
  Timeout?: number;
  ChecksumAlgorithm?: CheckSumAlgorithm;
}

export enum SignatureType {
  OBS = "obs",
  V2 = "v2",
  V4 = "v4",
}

// 列举对象
export interface ListObjectsInput extends BucketRequestInput {
  Prefix?: string;
  Marker?: string;
  MaxKeys?: number;
  Delimiter?: string;
  EncodingType?: string;
}

export interface ListObjectsOutput {
  Bucket: string;
  Location: string;
  Delimiter: string;
  IsTruncated: boolean;
  Prefix: string;
  Marker: string;
  NextMarker: string;
  MaxKeys: number;
  EncodingType: string;
  Contents: Content[];
  CommonPrefixes?: CommonPrefix[];
}

export interface CommonPrefix {
  Prefix: string;
}

export interface Content {
  Key: string;
  LastModified: string;
  ETag: string;
  Size: string;
  Type: string;
  StorageClass: string;
  Owner: Owner;
}
// 列举多版本对象
export interface ListVersionsInput extends BucketRequestInput {
  Prefix?: string;
  KeyMarker?: string;
  VersionIdMarker?: string;
  MaxKeys?: number;
  Delimiter?: string;
  EncodingType?: string;
}

export interface ListVersionsOutput {
  Bucket: string;
  Location: string;
  Delimiter: string;
  Prefix: string;
  KeyMarker: string;
  VersionIdMarker: string;
  NextKeyMarker: string;
  NextVersionIdMarker: string;
  MaxKeys: number;
  IsTruncated: boolean;
  EncodingType?: string;
  Versions: Version[];
  DeleteMarkers: DeleteMarker[];
  CommonPrefixes: CommonPrefix[];
}

export interface Version {
  Key: string;
  VersionId: string;
  IsLatest: boolean;
  LastModified: string;
  ETag: string;
  Size: string;
  Type: string;
  StorageClass: string;
  Owner: Owner;
}

export interface DeleteMarker {
  Key: string;
  VersionId: string;
  IsLatest: boolean;
  LastModified: string;
  Owner: Owner;
}

export interface CommonPrefix {
  Prefix: string;
  MTime: string;
  InodeNo: string;
}

// 桶网站配置
export interface SetBucketWebsiteInput extends BucketRequestInput {
  RedirectAllRequestsTo?: RedirectAllRequestsTo;
  IndexDocument?: IndexDocument;
  ErrorDocument?: ErrorDocument;
  RoutingRules?: RoutingRule[];
}

export interface RedirectAllRequestsTo {
  HostName: string;
  Protocol?: string;
}

export interface IndexDocument {
  Suffix: string;
}

export interface ErrorDocument {
  Key: string;
}

export interface RoutingRule {
  Condition?: Condition;
  Redirect: Redirect;
}

export interface Condition {
  HttpErrorCodeReturnedEquals?: number;
  KeyPrefixEquals?: string;
}

export interface Redirect {
  HostName?: string;
  HttpRedirectCode?: number;
  Protocol?: string;
  ReplaceKeyPrefixWith?: string;
  ReplaceKeyWith?: string;
}

export interface GetBucketWebsiteOutput {
  RedirectAllRequestsTo: RedirectAllRequestsTo;
  IndexDocument: IndexDocument;
  ErrorDocument: ErrorDocument;
  RoutingRules: RoutingRule[];
}

// 桶Cor规则
export interface SetBucketCorsInput extends BucketRequestInput {
  CorsRules: CorsRule[];
}

export interface CorsRule {
  ID: string;
  AllowedMethod: string[];
  AllowedOrigin: string[];
  AllowedHeader: string[];
  MaxAgeSeconds: number;
  ExposeHeader: string[];
}

export interface GetBucketCorsOutput {
  CorsRules: CorsRule[];
}

// 桶生命周期
export interface SetBucketLifecycleInput extends BucketRequestInput {
  Rules: LifecycleRule[];
}

export interface LifecycleRule {
  ID?: string;
  Prefix: string;
  Status: string;
  Transitions?: Transitions[];
  Expiration?: Expiration;
  NoncurrentVersionTransitions?: NoncurrentVersionTransition[];
  NoncurrentVersionExpiration?: NoncurrentVersionExpiration;
  AbortIncompleteMultipartUpload?: AbortIncompleteMultipartUpload;
  Filter?: LifecycleFilter;
}

export interface Transitions {
  StorageClass: StorageClassType;
  Date?: string;
  Days?: number;
}

export interface Expiration {
  Date?: string;
  Days?: number;
  ExpiredObjectDeleteMarker?: string;
}

export interface NoncurrentVersionTransition {
  StorageClass: StorageClassType;
  NoncurrentDays: number;
}

export interface NoncurrentVersionExpiration {
  NoncurrentDays: number;
}

export interface AbortIncompleteMultipartUpload {
  DaysAfterInitiation: number;
}

export interface LifecycleFilter {
  Prefix: string;
  Tags: Tag[];
}

export interface GetBucketLifecycleOutput {
  Rules: LifecycleRule[];
}

// 桶多版本
export interface SetBucketVersioningInput extends BucketRequestInput {
  VersionStatus: VersioningStatus;
}

export interface GetBucketVersioningOutput {
  VersionStatus: VersioningStatus;
}

// 存储类别
export interface SetBucketStorageClassInput extends BucketRequestInput {
  StorageClass: StorageClassType;
}

export interface GetBucketStorageClassOutput {
  StorageClass: StorageClassType;
}

// 桶日志
export interface SetBucketLoggingInput extends BucketRequestInput {
  Agency?: string;
  LoggingEnabled: LoggingEnabled;
}

export interface LoggingEnabled {
  TargetBucket: string;
  TargetPrefix: string;
  TargetGrants: Grant[];
}

export interface GetBucketLoggingOutput {
  Agency: string;
  LoggingEnabled: LoggingEnabled;
}

// 桶标签
export interface SetBucketTaggingInput extends BucketRequestInput {
  Tags: Tag[];
}

export interface Tag {
  Key: string;
  Value: string;
}

export interface GetBucketTaggingOutput {
  Tags: Tag[];
}

// 桶配额
export interface SetBucketQuotaInput extends BucketRequestInput {
  StorageQuota: number;
}

export interface GetBucketQuotaOutput {
  StorageQuota: number;
}

// 桶存量信息
export interface GetBucketStorageInfoOutput {
  Size: number;
  ObjectNumber: number;
  StandardSize?: number;
  StandardObjectNumber?: number;
  Standard_IASize?: number;
  Standard_IAObjectNumber?: number;
  GlacierObjectNumber?: number;
  DeepArchiveSize?: number;
  DeepArchiveObjectNumber?: number;
  HighPerformanceSize?: number;
  HighPerformanceObjectNumber?: number;
}

// 桶清单
export interface SetBucketReplicationInput extends BucketRequestInput {
  Agency: string;
  Rules: ReplicationRule[];
}

export interface ReplicationRule {
  ID?: string;
  Prefix: string;
  Status: string;
  Destination: Destination;
  HistoricalObjectReplication?: string;
}

export interface Destination {
  Bucket: string;
  StorageClass?: string;
  DeleteData?: string;
}
export interface GetBucketReplicationOutput {
  Agency: string;
  Rules: ReplicationRule[];
}

// 桶加密X
export interface SetBucketEncryptionInput extends BucketRequestInput {
  Rule: BucketEncryptionRule;
}

export interface BucketEncryptionRule {
  ApplyServerSideEncryptionByDefault: ApplyServerSideEncryptionByDefault
}

export interface ApplyServerSideEncryptionByDefault {
  SSEAlgorithm: string;
  KMSMasterKeyID?: string;
  ProjectID?: string;
  KMSDataEncryption?: string;
}

export interface GetBucketEncryptionOutput {
  Rule: BucketEncryptionRule;
}

export interface RequestParams {
  RequestDate?: string;
}

export interface BaseResponseOutput {
  RequestId: string;
  Id2: string;
}

export interface BucketRequestInput extends RequestParams {
  Bucket: string;
}

export interface CreateBucketInput extends BucketRequestInput {
  Acl?: AclType;
  StorageClass?: StorageClassType;
  ObjectLockEnabled?: string;
  IESLocation?: string;
  FileInterface?: string;
  BucketType?: BucketType;
  AzRedundancy?: string;
  Redundancy?: string;
  IsFusionAllowUpgrade?: string;
  IsFusionAllowAlternative?: string;
  Cluster?: string;
  GrantFullControl?: string;
  GrantFullControlDelivered?: string;
  GrantRead?: string;
  GrantReadDelivered?: string;
  GrantReadACP?: string;
  GrantWrite?: string;
  GrantWriteACP?: string;
  ClusterType?: string;
  MultiEnterprise?: string;
  Location?: string; // XML
  SseKms?: string;
  SseAlgorithm?: string;
  SseKmsKey?: string;
  SseKmsProjectId?: string;
}

export interface GetBucketMetadataInput extends BucketRequestInput{
  Origin?: string;
  RequestHeader?: string;
}

export interface GetBucketMetadataOutput {
  StorageClass: StorageClassType;
  ObsVersion: string;
  Location: string;
  FileInterface: string;
  BucketType: string;
  AzRedundancy: string;
  Redundancy: string;
  Cluster: string;
  MultiEnterprise: string;
  ClusterType: string;
  IESLocation: string;
  AllowOrigin: string;
  MaxAgeSeconds: string;
  ExposeHeader: string;
  AllowMethod: string;
  AllowHeader: string;
}

export interface GetBucketLocationOutput {
  Location: string;
}

export interface SetBucketPolicyInput extends BucketRequestInput {
  Policy: string;
}

export interface GetBucketPolicyOutput {
  Policy: string;
}

// 桶Acl
export interface SetBucketAclInput extends BucketRequestInput {
  Acl?: AclType;
  Owner?: Owner;
  Grants?: Grant[];
}

export interface Grant {
  Grantee?: Grantee;
  Permission?: PermissionType;
  Delivered?: boolean;
}

export interface Grantee {
  Type?: GranteeType;
  ID?: string;
  Name?: string;
  URI?: string;
}

export interface GetBucketAclOutput {
  Owner: Owner;
  Grants: Grant[];
}

export interface ListBucketsInput {
  BucketType?: BucketType;
  QueryLocation?: boolean;
}

export interface Owner {
  ID: string;
  DisplayName?: string;
}

export interface Bucket {
  Name: string;
  CreationDate: string;
  Location: string;
  BucketType: string;
  ClusterType?: string;
  IESLocation?: string;
}

export interface ListBucketsOutput {
  Buckets: Bucket[];
  Owner: Owner;
}

// 自定义域名
export interface SetBucketCustomDomainInput extends BucketRequestInput {
  DomainName: string;
}

export interface GetBucketCustomDomainInput extends BucketRequestInput {
}

export interface GetBucketCustomDomainOutput extends BucketRequestInput {
  Domains: Domain[];
}

export interface DeleteBucketCustomDomainInput extends BucketRequestInput {
  DomainName: string;
}

export interface Domain {
  DomainName: string;
  Value: string;
}

// 归档直读
export interface SetBucketDirectColdAccessInput extends BucketRequestInput {
  Status: string;
}

export interface GetBucketDirectColdAccessOutput extends BucketRequestInput {
  Status: string;
}

// 镜像回源接口
export interface SetBucketMirrorBackToSourceInput extends BucketRequestInput {
  Rules: string;
}

export interface GetBucketMirrorBackToSourceOutput {
  Rules: string;
}

// 桶清单接口
export interface SetBucketInventoryInput extends BucketRequestInput {
  Id: string;
  InventoryConfiguration: InventoryConfiguration
}

export interface GetBucketInventoryInput extends BucketRequestInput {
  Id: string;
}

export interface GetBucketInventoryOutput{
  Rules: InventoryConfiguration[]
}

export interface DeleteBucketInventoryInput extends BucketRequestInput {
  Id: string;
}

export interface InventoryConfiguration {
  Id: string;
  IsEnabled: Boolean;
  Filter?: InventoryFilter;
  Schedule: InventorySchedule;
  Destination: InventoryDestination;
  IncludedObjectVersions: IncludedObjectVersionsType;
  OptionalFields?: OptionalFields;
}

export interface InventoryDestination {
  Format: string;
  Bucket: string;
  Prefix?: string;
}

export interface InventoryFilter {
  Prefix?: string;
}

export interface InventorySchedule {
  Frequency: FrequencyType;
}

export interface OptionalFields {
  Field?: string[];
}

export interface ObjectRequestInput extends BucketRequestInput {
  Key: string;
}

export interface ObjectVersionInput extends ObjectRequestInput {
  VersionId?: string;
}

export interface HttpHeader {
  HttpExpires?: number;
  CacheControl?: string;
  ContentType?: string;
  ContentDisposition?: string;
  ContentLanguage?: string;
  ContentEncoding?: string;
}

export interface SseKmsHeader {
  SseKms?: string;
  SseKmsKey?: string;
  SseKmsProjectId?: string;
}

export interface SseCHeader {
  SseC?: string;
  SseCKey?: string;
  SseCKeyMd5?: string;
}

export interface ObjectEncryptionRule extends SseKmsHeader, SseCHeader {
  SseMode?: string;
  SseAlgorithm?: string;
}

export interface ObjectOperationInput extends ObjectRequestInput, HttpHeader, ObjectEncryptionRule {
  Acl?: AclType;
  GrantRead?: string;
  GrantReadAcp?: string;
  GrantWriteAcp?: string;
  GrantFullControl?: string;
  StorageClass?: StorageClassType;
  Metadata?: object;
  WebsiteRedirectLocation?: string;
  Tags?: string;
  Expires?: number;
  ObjectLockMode?: string;
  ObjectLockRetainUntailDate?: string;
  EncodingType?: string;
}

export interface PutObjectBasicInput extends ObjectOperationInput {
  ContentLength?: string;
  ContentMD5?: string;
  ContentSHA256?: string;
}

export interface PutObjectInput extends PutObjectBasicInput {
  Body?: string | ArrayBuffer;
  SourceFile?: string; 
}

export interface PutObjectOutput extends ObjectEncryptionRule {
  ETag: string;
  VersionId?: string;
  StorageClass?: StorageClassType;
}


export interface AppendObjectInput extends PutObjectBasicInput {
  Offset?: number;
  Position: number;
  Body: string;
}

export interface AppendObjectOutput extends ObjectEncryptionRule {
  ETag: string;
  StorageClass: StorageClassType;
  NextPosition: number;
}

export interface DeleteObjectOutput {
  VersionId: string;
  DeleteMarker: boolean;
}


export interface DeleteObjectsInput extends BucketRequestInput {
  Quiet?: boolean;
  Objects: ObjectToDelete[];
  EncodingType?: string;
}

export interface ObjectToDelete {
  Key: string;
  VersionId?: string;
}

export interface DeleteObjectsOutput {
  Deleteds?: Deleted[];
  Errors?: Error[];
  EncodingType?: string;
}

export interface Deleted {
  Key: string;
  VersionId?: string;
  DeleteMarker?: string;
  DeleteMarkerVersionId?: string;
}

export interface Error {
  Key: string;
  VersionId?: string;
  Code?: string;
  Message?: string;
}

export interface RestoreObjectInput extends ObjectVersionInput {
  Days: number;
  Tier: RestoreTierType;
}

export interface GetObjectMetadataInput extends ObjectVersionInput, SseCHeader {

}

export interface GetObjectMetadataOutput extends HttpHeader, ObjectEncryptionRule {
  ObjectLockMode: string;
  ObjectLockRetainUntilDate: string;
  Expiration: string;
  LastModified: string;
  ContentLength: string;
  ContentType: string;
  TaggingCount: string;
  ETag: string;
  VersionId: string;
  WebsiteRedirectLocation: string;
  StorageClass: StorageClassType;
  Restore: string;
  ObjectType: string;
  NextPosition: string;
  AllowOrigin: string;
  MaxAgeSeconds: string;
  ExposeHeader: string;
  AllowMethod: string;
  AllowHeader: string;
  Metadata: object;
  ReplicationStatus: string;
}

export interface SetObjectMetadataInput extends ObjectVersionInput, HttpHeader {
  Tags?: string;
  Expires?: string;
  Metadata?: object;
  StorageClass?: StorageClassType;
  WebsiteRedirectLocation?: string;
  MetadataDirective: MetadataDirectiveType;
}

export interface SetObjectMetadataOutput extends HttpHeader {
  ContentType: string;
  ContentLength: string;
  WebsiteRedirectLocation: string;
  MetadataDirective: MetadataDirectiveType;
  StorageClass: StorageClassType;
  Metadata: object
}

export interface GetObjectMetadata extends ObjectVersionInput {

}


export interface CopyObjectInput extends ObjectOperationInput, HttpHeader {
  CopySource: string;
  CopySourceIfMatch?: string;
  CopySourceIfModifiedSince?: string;
  CopySourceIfNoneMatch?: string;
  CopySourceIfUnmodifiedSince?: string;
  MetadataDirective?: MetadataDirectiveType;
  TagDirective?: string;
  CopySourceSseC?: string;
  CopySourceSseCKey?: string;
}

export interface CopyObjectOutput extends ObjectEncryptionRule {
  VersionId: string;
  CopySourceVersionId: string;
  ETag: string;
  LastModified: string;
}

export interface SetObjectAclInput extends ObjectVersionInput {
  Acl?: AclType,
  Delivered?: string;
  Owner?: Owner;
  Grants?: Grant[];
}

export interface GetObjectAclOutput {
  VersionId: string;
  Delivered: string;
  Owner: Owner;
  Grants: Grant[];
}

export interface GetObjectTaggingOutput {
  Tags: Tag[];
}

export interface SetObjectTaggingInput extends ObjectVersionInput {
  Tags: Tag[];
}

export interface Tag {
  Key: string;
  Value: string;
}

export interface SetObjectStorageClassInput extends ObjectVersionInput {
  StorageClass: StorageClassType;
}


export interface GetObjectStorageClassOutput {
  StorageClass: StorageClassType;
}

export interface ModifyObjectInput extends ObjectRequestInput {
  Position: number;
  Body: string;
}

export interface ModifyObjectOutput {
  ETag: string;
}

export interface TruncateObjectInput extends ObjectRequestInput {
  Length: number;
}

export interface RenameObjectInput extends ObjectRequestInput {
  NewObjectKey: string;
}

export interface SetObjectTaggingInput extends ObjectVersionInput {
  Tags: Tag[];
}

export interface GetObjectTaggingOutput {
  Tags: Tag[];
}

// 下载对象
export interface GetObjectInput extends ObjectRequestInput, SseCHeader {
  ResponseCacheControl?: string;
  ResponseContentDisposition?: string;
  ResponseContentEncoding?: string;
  ResponseContentLanguage?: string;
  ResponseContentType?: string;
  ResponseExpires?: string;
  VersionId?: string;
  ImageProcess?: string;
  IfMatch?: string;
  IfModifiedSince?: string;
  IfNoneMatch?: string;
  IfUnmodifiedSince?: string;
  Range?: string;
  Origin?: string;
  RequestHeader?: string;
  SaveByType?: SaveByType;
  SaveFilePath?: string;
}

export interface GetObjectOutput extends GetObjectMetadataOutput {
  Content?: string | ArrayBuffer;
  SaveFilePath?: string;
}

export interface InitiateMultipartUploadInput extends ObjectOperationInput {
  EncodingType?: string;
}

export interface InitiateMultipartUploadOutput extends PartRequestInput, ObjectEncryptionRule {
  EncodingType?: string;
}

export interface InitiateMultipartUploadOutput extends PartRequestInput, ObjectEncryptionRule {
  EncodingType?: string;
}

export interface ListMultipartUploadsInput extends BucketRequestInput {
  Prefix?: string;
  Delimiter?: string;
  KeyMarker?: string;
  UploadIdMarker?: string;
  MaxUploads?: number;
  EncodingType?: string;
}

export interface ListMultipartUploadsOutput {
  Bucket: string;
  KeyMarker: string;
  UploadIdMarker: string;
  NextKeyMarker: string;
  Prefix: string;
  Delimiter: string;
  NextUploadIdMarker: string;
  MaxUploads: string;
  IsTruncated: boolean;
  EncodingType?: string;
  Uploads: Upload[];
}

export interface Upload {
  UploadId: string;
  Key: string;
  Initiated: string;
  StorageClass: StorageClassType;
  Owner: Owner;
  Initiator: Initiator;
}

export interface Initiator {
  ID: string;
  DisplayName?: string;
}

export interface UploadPartInput extends PartRequestInput, SseKmsHeader, SseCHeader {
  PartNumber: number;
  ContentMD5?: string;
  ContentSHA256?: string;
  Body: string | ArrayBuffer;
  Offset?: string;
  PartSize?: number;
}

export interface UploadPartOutput {
  ETag: string;
}

export interface ListPartsInput extends PartRequestInput {
  MaxParts?: number;
  PartNumberMarker?: number;
  EncodingType?: string;
}

export interface ListPartsOutput extends PartRequestInput {
  PartNumberMarker: number;
  NextPartNumberMarker: number;
  MaxParts: number;
  IsTruncated: boolean;
  StorageClass: StorageClassType;
  EncodingType?: string;
  Initiator: Initiator;
  Owner: Owner;
  Parts: ListPart[];
}

export interface CopyPartInput extends PartRequestInput, SseCHeader {
  PartNumber: number;
  CopySource: string
  CopySourceRange?: string
}

export interface CopyPartOutput extends ObjectEncryptionRule {
  LastModified: string;
  ETag: string;
}

export interface PartRequestInput extends ObjectRequestInput {
  UploadId: string;
}

export interface AbortMultipartUploadInput extends PartRequestInput {

}

export interface CompleteMultipartUploadInput extends PartRequestInput,ObjectEncryptionRule {
  EncodingType?: string;
  Parts?: Part[];
  Callback?: ICallback
}

export interface ICallback {
  CallbackUrl: string;
  CallbackBody: string;
  CallbackHost?: string;
  CallbackBodyType?: string;
}

export interface Part {
  PartNumber: number;
  ETag: string;
}

export interface ListPart extends Part {
  LastModified?: string;
  Size?: string;
}

export interface CompleteMultipartUploadOutput extends ObjectVersionInput, SseKmsHeader, SseCHeader {
  Location: string;
  EncodingType: string;
  ETag: string;
  CallbackResponse?: any
}

export enum SaveByType {
  TEXT = "string",
  ARRAY_BUFFER = "array_buffer",
  OBJECT = "object",
  FILE = "file",
}

export enum HttpMethodType {
  GET = "GET",
  POST = "POST",
  PUT = "PUT",
  DELETE = "DELETE",
  HEAD = "HEAD",
  OPTIONS = "OPTIONS",
}

export enum SpecialParam {
  VERSIONS = "versions",
  UPLOADS = "uploads",
  LOCATION = "location",
  STORAGE_INFO = "storageinfo",
  QUOTA = "quota",
  STORAGE_POLICY = "storagePolicy",
  ACL = "acl",
  APPEND = "append",
  LOGGING = "logging",
  POLICY = "policy",
  LIFECYCLE = "lifecycle",
  WEBSITE = "website",
  VERSIONING = "versioning",
  CORS = "cors",
  NOTIFICATION = "notification",
  TAGGING = "tagging",
  DELETE = "delete",
  RESTORE = "restore",
}

export interface CreateSignedUrlSyncInput {
  Method: HttpMethodType;
  Bucket?: string;
  Key?: string;
  SpecialParam?: SpecialParam;
  Expires?: number;
  Headers?: object;
  QueryParams?: object;
}

export interface AfterStringToSignOutput {
  signature: string;
  ak: string;
  stsToken?: string;
}

export interface CreateSignedUrlInput extends CreatePostSignatureSyncInput {
  AfterStringToSign: (stringToSign: string) => Promise<AfterStringToSignOutput>;
}

export interface CreateSignedUrlSyncOutput {
  SignedUrl: string;
  ActualSignedRequestHeaders: object;
}

export interface CreateSignedUrlOutput extends CreateSignedUrlSyncOutput{
}


export interface CreatePostSignatureSyncInput {
  Bucket?: string;
  Key?: string;
  Expires?: number;
  FormParams?: object;
}

export interface CreatePostSignatureSyncOutput {
  OriginPolicy: string;
  Policy: string;
  Signature: string;
}

export interface ICommonMsg extends BaseResponseOutput {
  Status: number;
  Code: string;
  Message: string;
  HostId: string;
  Indicator: string;
}

export interface IResponseOuput<T> {
  CommonMsg: ICommonMsg;
  InterfaceResult: T;
}

type Response<T> = Promise<IResponseOuput<T>>;

declare class ObsClient {
  constructor(config: ObsClientConfig);
  initLog(logConfig: LogConfig);
  createBucket(input: CreateBucketInput): Response<BaseResponseOutput>;
  headBucket(input: BucketRequestInput): Response<BaseResponseOutput>;
  listBuckets(input: ListBucketsInput): Response<ListBucketsOutput>;
  deleteBucket(input: BucketRequestInput): Response<BaseResponseOutput>;
  getBucketMetadata(input: BucketRequestInput): Response<GetBucketMetadataOutput>;
  getBucketLocation(input: BucketRequestInput): Response<GetBucketLocationOutput>;

  // 列举对象
  listObjects(input: ListObjectsInput): Response<ListObjectsOutput>;

  // 列举多版本对象
  listVersions(input: ListVersionsInput): Response<ListVersionsOutput>;
  
  // 桶策略相关接口
  setBucketPolicy(input: SetBucketPolicyInput): Response<BaseResponseOutput>;
  getBucketPolicy(input: BucketRequestInput): Response<GetBucketPolicyOutput>;
  deleteBucketPolicy(input: BucketRequestInput): Response<BaseResponseOutput>;

  // 桶Acl相关接口
  setBucketAcl(input: SetBucketAclInput): Response<BaseResponseOutput>;
  getBucketAcl(input: BucketRequestInput): Response<GetBucketAclOutput>;

  // 桶日志相关接口
  setBucketLogging(input: SetBucketLoggingInput): Response<BaseResponseOutput>;
  getBucketLogging(input: BucketRequestInput): Response<GetBucketLoggingOutput>;

  // 桶生命周期相关接口
  setBucketLifecycle(input: SetBucketLifecycleInput): Response<BaseResponseOutput>;
  getBucketLifecycle(input: BucketRequestInput): Response<GetBucketLifecycleOutput>;
  deleteBucketLifecycle(input: BucketRequestInput): Response<BaseResponseOutput>;

  // 桶多版本状态相关接口
  setBucketVersioning(input: SetBucketVersioningInput): Response<BaseResponseOutput>;
  getBucketVersioning(input: BucketRequestInput): Response<GetBucketVersioningOutput>;

  // 桶默认存储类别相关接口
  setBucketStoragePolicy(input: SetBucketStorageClassInput): Response<BaseResponseOutput>;
  getBucketStoragePolicy(input: BucketRequestInput): Response<GetBucketStorageClassOutput>;

  // 桶跨区域复制相关接口
  setBucketReplication(input: SetBucketReplicationInput): Response<BaseResponseOutput>;
  getBucketReplication(input: BucketRequestInput): Response<GetBucketReplicationOutput>;
  deleteBucketReplication(input: BucketRequestInput): Response<BaseResponseOutput>;

  // 桶标签相关接口
  setBucketTagging(input: SetBucketTaggingInput): Response<BaseResponseOutput>;
  getBucketTagging(input: BucketRequestInput): Response<GetBucketTaggingOutput>;
  deleteBucketTagging(input: BucketRequestInput): Response<BaseResponseOutput>;

  // 桶配额相关接口
  setBucketQuota(input: SetBucketQuotaInput): Response<BaseResponseOutput>;
  getBucketQuota(input: BucketRequestInput): Response<GetBucketQuotaOutput>;

  // 桶存量相关接口
  getBucketStorageInfo(input: BucketRequestInput): Response<GetBucketStorageInfoOutput>;

  // 桶清单相关接口
  setBucketInventory(input: SetBucketInventoryInput): Response<BaseResponseOutput>;
  getBucketInventory(input: GetBucketInventoryInput): Response<GetBucketInventoryOutput>;
  deleteBucketInventory(input: DeleteBucketInventoryInput): Response<BaseResponseOutput>;

  // 自定义域名相关接口
  setBucketCustomDomain(input: SetBucketCustomDomainInput): Response<BaseResponseOutput>;
  getBucketCustomDomain(input: BucketRequestInput): Response<GetBucketCustomDomainOutput>;
  deleteBucketCustomDomain(input: DeleteBucketCustomDomainInput): Response<BaseResponseOutput>;

  // 桶加密相关接口
  setBucketEncryption(input: SetBucketEncryptionInput): Response<BaseResponseOutput>;
  getBucketEncryption(input: BucketRequestInput): Response<GetBucketEncryptionOutput>;
  deleteBucketEncryption(input: BucketRequestInput): Response<BaseResponseOutput>;

  // 归档直读相关接口
  setBucketDirectColdAccess(input: SetBucketDirectColdAccessInput): Response<BaseResponseOutput>;
  getBucketDirectColdAccess(input: BucketRequestInput): Response<GetBucketDirectColdAccessOutput>;
  deleteBucketDirectColdAccess(input: BucketRequestInput): Response<BaseResponseOutput>;

  // 镜像回源相关接口
  setBucketMirrorBackToSourceInput(input: SetBucketMirrorBackToSourceInput): Response<BaseResponseOutput>;
  getBucketMirrorBackToSourceIntput(input: BucketRequestInput): Response<GetBucketMirrorBackToSourceOutput>;
  deleteBucketMirrorBackToSourceIntput(input: BucketRequestInput): Response<BaseResponseOutput>;

  // 桶跨域规则相关接口
  setBucketCors(input: SetBucketCorsInput): Response<BaseResponseOutput>;
  getBucketCors(input: BucketRequestInput): Response<GetBucketCorsOutput>;
  deleteBucketCors(input: BucketRequestInput): Response<BaseResponseOutput>;

  // 桶网站配置相关接口
  setBucketWebsite(input: SetBucketWebsiteInput): Response<BaseResponseOutput>;
  getBucketWebsite(input: BucketRequestInput): Response<GetBucketWebsiteOutput>;
  deleteBucketWebsite(input: BucketRequestInput): Response<BaseResponseOutput>;

  // 上传对象接口
  putObject(input: PutObjectInput): Response<PutObjectOutput>;
  appendObject(input: AppendObjectInput): Response<AppendObjectOutput>;
  copyObject(input: CopyObjectInput): Response<CopyObjectOutput>;

  // 对象元数据相关接口
  setObjectMetadata(input: SetObjectMetadataInput): Response<SetObjectMetadataOutput>;
  getObjectMetadata(input: GetObjectMetadataInput): Response<GetObjectMetadataOutput>;

  // 对象Acl相关接口
  setObjectAcl(input: SetObjectAclInput): Response<BaseResponseOutput>;
  getObjectAcl(input: ObjectVersionInput): Response<GetObjectAclOutput>;

  // 对象存储类别相关接口-browserjs 缺少接口
  setObjectStorageClass(input: SetObjectStorageClassInput): Response<BaseResponseOutput>;
  getObjectStorageClass(input: ObjectVersionInput): Response<GetObjectStorageClassOutput>;

  // 对象标签相关接口
  setObjectTagging(input: SetObjectTaggingInput): Response<BaseResponseOutput>;
  getObjectTagging(input: ObjectVersionInput): Response<GetObjectTaggingOutput>;
  deleteObjectTagging(input: ObjectVersionInput): Response<BaseResponseOutput>;

  // 下载对象
  getObject(input: GetObjectInput): Response<GetObjectOutput>;

  // 取回对象
  restoreObject(input: RestoreObjectInput): Response<BaseResponseOutput>;

  // 修改写对象
  modifyObject(input: ModifyObjectInput): Response<ModifyObjectOutput>;

  // 截断对象
  truncateObject(input: TruncateObjectInput): Response<BaseResponseOutput>;

  // 重命名对象
  renameObject(input: RenameObjectInput): Response<BaseResponseOutput>;

  // 删除对象
  deleteObject(input: ObjectVersionInput): Response<DeleteObjectOutput>;

  // 批量删除对象
  deleteObjects(input: DeleteObjectsInput): Response<DeleteObjectsOutput>;

  // 段相关接口
  initiateMultipartUpload(input: InitiateMultipartUploadInput): Response<InitiateMultipartUploadOutput>;
  listParts(input: ListPartsInput): Response<ListPartsOutput>;
  listMultipartUploads(input: ListMultipartUploadsInput): Response<ListMultipartUploadsOutput>;
  uploadPart(input: UploadPartInput): Response<UploadPartOutput>;
  copyPart(input: CopyPartInput): Response<CopyPartOutput>;
  abortMultipartUpload(input: AbortMultipartUploadInput): Response<BaseResponseOutput>;
  completeMultipartUpload(input: CompleteMultipartUploadInput): Response<CompleteMultipartUploadOutput>;

  // 高阶接口
  createSignedUrlSync(input: CreateSignedUrlSyncInput): CreateSignedUrlSyncOutput;
  createSignedUrl(input: CreateSignedUrlInput): Response<CreateSignedUrlOutput>;
  createPostSignatureSync(input: CreatePostSignatureSyncInput): CreatePostSignatureSyncOutput;
}

export default ObsClient;
