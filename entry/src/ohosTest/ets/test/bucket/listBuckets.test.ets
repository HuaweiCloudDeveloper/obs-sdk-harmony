import { BucketType } from '@obs/esdk-obs-harmony';
import { describe, it, expect, beforeAll, afterAll } from '@ohos/hypium';
import { EXPECT_CODE, NO_CONTENT_CODE } from '../common/const';
import { DEFAULT_LOCATION, getObsClient } from '../common/testUtil';

const bucketName = 'obs-sdk-test-bucket-for-listbuckets'
const posixBucketName = 'obs-sdk-test-bucket-for-listbuckets-posix'

export default function listBuckets() {
  describe("listBuckets", () => {
    beforeAll(async () => {
      const result = await getObsClient().createBucket({
        Bucket: bucketName,
        Location: DEFAULT_LOCATION
      })
      expect(result.CommonMsg.Status).assertEqual(EXPECT_CODE)
      const result2 = await getObsClient().createBucket({
        Bucket: posixBucketName,
        Location: DEFAULT_LOCATION
      })
      expect(result2.CommonMsg.Status).assertEqual(EXPECT_CODE)
    });
    afterAll(async () => {
      const result = await getObsClient().deleteBucket({
        Bucket: bucketName
      })
      expect(result.CommonMsg.Status).assertEqual(NO_CONTENT_CODE)
      const result2 = await getObsClient().deleteBucket({
        Bucket: posixBucketName
      })
      expect(result2.CommonMsg.Status).assertEqual(NO_CONTENT_CODE)
    });
    it("tc_list_buckets_001", 0, async () => { //不带参数列举桶
      const result = await getObsClient().listBuckets({})
      expect(result.CommonMsg.Status).assertEqual(EXPECT_CODE);
      expect(result.InterfaceResult.Buckets.length).assertLarger(0);
    });
    it("tc_list_buckets_002", 0, async () => { // 指定查询桶区域参数列举桶
      const result = await getObsClient().listBuckets({
        QueryLocation: true
      })
      expect(result.CommonMsg.Status).assertEqual(EXPECT_CODE);
      expect(result.InterfaceResult.Buckets.length).assertLarger(0);
    });
    it("tc_list_buckets_003", 0, async () => { // 指定BucketType分别为OBJECT和POSIX参数列举桶
      const result = await getObsClient().listBuckets({
        BucketType: BucketType.OBJECT
      })
      expect(result.CommonMsg.Status).assertEqual(EXPECT_CODE);
      expect(result.InterfaceResult.Buckets.length).assertLarger(0);
      for (let bucket of result.InterfaceResult.Buckets) {
        expect(bucket.BucketType).assertEqual(BucketType.OBJECT);
      }
      const result2 = await getObsClient().listBuckets({
        BucketType: BucketType.POSIX
      })
      expect(result2.CommonMsg.Status).assertEqual(EXPECT_CODE);
      expect(result2.InterfaceResult.Buckets.length).assertLarger(0);
      for (let bucket of result2.InterfaceResult.Buckets) {
        expect(bucket.BucketType).assertEqual(BucketType.POSIX);
      }
    });
    it("tc_list_buckets_004", 0, async () => { //不指定BucketType参数列举桶；
      const result = await getObsClient().listBuckets({})
      expect(result.CommonMsg.Status).assertEqual(EXPECT_CODE);
      expect(result.InterfaceResult.Buckets.length).assertLarger(0);
    });

  })
}