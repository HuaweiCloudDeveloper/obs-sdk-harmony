import { Tag } from '@obs/esdk-obs-harmony';
import { describe, it, expect, beforeAll, afterAll } from '@ohos/hypium';
import {
  EXPECT_CODE,
  NO_CONTENT_CODE,
  NO_EXIST_BUCKET_NAME,
  REQUEST_ERROR_CODE,
  SERVER_ERROR_CODE
} from '../common/const';
import { DEFAULT_LOCATION, getObsClient } from '../common/testUtil';
import { randomString } from '../common/helper';

const bucketName = 'obs-sdk-test-bucket-for-buckettagging'

export default function BucketTagging() {
  describe("BucketTagging", () => {
    beforeAll(async () => {
      const result = await getObsClient().createBucket({
        Bucket: bucketName,
        Location: DEFAULT_LOCATION
      })
      expect(result.CommonMsg.Status).assertEqual(EXPECT_CODE)
    });
    afterAll(async () => {
      const result = await getObsClient().deleteBucket({
        Bucket: bucketName
      })
      expect(result.CommonMsg.Status).assertEqual(NO_CONTENT_CODE)
    });
    it("tc_set_bucket_tagging_001", 0, async () => { //传入正确的桶名，有效的标签配置，设置桶标签成功
      const result = await getObsClient().setBucketTagging({
        Bucket: bucketName,
        Tags: [{
          Key: "TagNameJJ1", Value: "tytttasceettt"
        }]
      })
      expect(result.CommonMsg.Status).assertEqual(NO_CONTENT_CODE);
    });
    it("tc_set_bucket_tagging_002", 0, async () => { //传入正确的桶名，包含unicode的标签配置，设置桶标签成功
      const result = await getObsClient().setBucketTagging({
        Bucket: bucketName,
        Tags: [
          {
            Key: "TagNameJJ1", Value: "tytttasceettt"
          },
          {
            Key: "测试键", Value: "测试值"
          }
        ]
      })
      expect(result.CommonMsg.Status).assertEqual(NO_CONTENT_CODE);
    });
    it("tc_set_bucket_tagging_003", 0, async () => { //传入正确的桶名，空的标签配置，设置桶标签失败
      const result = await getObsClient().setBucketTagging({
        Bucket: bucketName,
        Tags: []
      })
      expect(result.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
    });
    it("tc_set_bucket_tagging_004", 0, async () => { //传入正确的桶名，无效标签配置（标签个数大于20个），设置桶标签失败
      let tag_list: Tag[] = []
      for (let i: number = 1; i <= 21; i++) {
        tag_list.push({
          Key: "TagNameJJ1" + i.toString(), Value: "tytttasceettt" + i.toString()
        })
      }
      const result = await getObsClient().setBucketTagging({
        Bucket: bucketName,
        Tags: tag_list
      })
      expect(result.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
    });
    it("tc_set_bucket_tagging_005", 0, async () => { //传入正确的桶名，无效标签配置（标签的Key值长度大于36），设置桶标签失败
      const result = await getObsClient().setBucketTagging({
        Bucket: bucketName,
        Tags: [{
          Key: randomString(37), Value: "tytttasceettt"
        }]
      })
      expect(result.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
    });
    it("tc_set_bucket_tagging_006", 0, async () => { //传入正确的桶名，无效标签配置（标签的Value值长度大于43），设置桶标签失败
      const result = await getObsClient().setBucketTagging({
        Bucket: bucketName,
        Tags: [{
          Key: "TagNameJJ1", Value: randomString(44)
        }]
      })
      expect(result.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
    });
    it("tc_set_bucket_tagging_007", 0, async () => { //传入不存在的桶名，设置桶标签失败
      const result = await getObsClient().setBucketTagging({
        Bucket: NO_EXIST_BUCKET_NAME,
        Tags: [{
          Key: "TagNameJJ1", Value: "tytttasceettt"
        }]
      })
      expect(result.CommonMsg.Status).assertEqual(SERVER_ERROR_CODE);
    });
    it("tc_get_bucket_tagging_001", 0, async () => { //传入正确的桶名，获取桶标签成功
      // 设置标签
      const setBucketTaggingOutput = await getObsClient().setBucketTagging({
        Bucket: bucketName,
        Tags: [{
          Key: "TagNameJJ1", Value: "tytttasceettt"
        }]
      })
      expect(setBucketTaggingOutput.CommonMsg.Status).assertEqual(NO_CONTENT_CODE);
      // 获取标签
      const getBucketTaggingOutput = await getObsClient().getBucketTagging({
        Bucket: bucketName
      })
      expect(getBucketTaggingOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      expect(getBucketTaggingOutput.InterfaceResult.Tags[0].Key).assertEqual("TagNameJJ1");
      expect(getBucketTaggingOutput.InterfaceResult.Tags[0].Value).assertEqual("tytttasceettt");
    });
    it("tc_get_bucket_tagging_003", 0, async () => { //传入不存在的桶名，获取桶标签失败
      const result = await getObsClient().getBucketTagging({
        Bucket: NO_EXIST_BUCKET_NAME
      })
      expect(result.CommonMsg.Status).assertEqual(SERVER_ERROR_CODE);
    });
    it("tc_delete_bucket_tagging_001", 0, async () => {
      const result = await getObsClient().deleteBucketTagging({
        Bucket: bucketName
      })
      expect(result.CommonMsg.Status).assertEqual(NO_CONTENT_CODE);
    });
    it("tc_delete_bucket_tagging_002", 0, async () => {
      const result = await getObsClient().deleteBucketTagging({
        Bucket: NO_EXIST_BUCKET_NAME
      })
      expect(result.CommonMsg.Status).assertEqual(SERVER_ERROR_CODE);
    });
    it("tc_get_bucket_tagging_002", 0, async () => { // 桶未设置标签，传入正确的桶名，获取桶标签失败
      const result = await getObsClient().getBucketTagging({
        Bucket: bucketName
      })
      expect(result.CommonMsg.Status).assertEqual(SERVER_ERROR_CODE);
    });
  })
}