import { describe, it, expect, afterAll, beforeAll } from '@ohos/hypium';
import {
  assertIsCreateBucketSuccess,
  assertIsDeleteBucketSuccess,
  assertIsUploadObjectSuccess
} from '../common/helper';
import {
  ErrCode,
  EXPECT_CODE,
  NO_EXIST_BUCKET_NAME,
  NO_EXIST_OBJECT_KEY,
  REQUEST_ERROR_CODE,
  SERVER_ERROR_CODE,
  SIMPLE_OBJECT_KEY,
  SPECIAL_OBJECT_KEY
} from '../common/const';
import { getObsClient, USER_ID, OWNER_ID } from '../common/testUtil';
import { AclType, GranteeType, PermissionType } from '@obs/esdk-obs-harmony';

const SIMPLE_BUCKET_NAME = 'obs-sdk-test-bucket-for-set-object-acl';

export default function setObjectAcl() {
  describe("test_set_object_acl", () => {
    beforeAll(async () => {
      await assertIsCreateBucketSuccess(SIMPLE_BUCKET_NAME);

      const putObjectOutput = await getObsClient().putObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY
      });
      expect(putObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      assertIsUploadObjectSuccess(SIMPLE_BUCKET_NAME, SIMPLE_OBJECT_KEY)

      const putObjectOutput2 = await getObsClient().putObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: SPECIAL_OBJECT_KEY
      });
      expect(putObjectOutput2.CommonMsg.Status).assertEqual(EXPECT_CODE);
      assertIsUploadObjectSuccess(SIMPLE_BUCKET_NAME, SPECIAL_OBJECT_KEY)
    });
    afterAll(async () => {
      await assertIsDeleteBucketSuccess(SIMPLE_BUCKET_NAME);
    });
    // 传入正确的桶名，对象名和有效的ACL配置，设置对象的ACL成功
    it("tc_set_object_acl_001", 0, async () => {
      for (let Permission of Object.values(PermissionType)) {
        const setObjectAclOutput = await getObsClient().setObjectAcl({
          Bucket: SIMPLE_BUCKET_NAME,
          Key: SIMPLE_OBJECT_KEY,
          Owner: {
            ID: OWNER_ID
          },
          Grants: [
            {
              Grantee: {
                Type: GranteeType.CANONICAL_USER, ID: USER_ID
              }, Permission
            }
          ]
        });
        expect(setObjectAclOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      }
    });
    // 传入正确的桶名，对象名（对象名包含中文和特殊字符）和有效的ACL配置，设置对象的ACL成功
    it("tc_set_object_acl_002", 0, async () => {
      for (let Permission of Object.values(PermissionType)) {
        const setObjectAclOutput = await getObsClient().setObjectAcl({
          Bucket: SIMPLE_BUCKET_NAME,
          Key: SPECIAL_OBJECT_KEY,
          Owner: {
            ID: OWNER_ID
          },
          Grants: [
            {
              Grantee: {
                Type: GranteeType.CANONICAL_USER, ID: USER_ID
              }, Permission
            }
          ]
        });
        expect(setObjectAclOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      }
    });
    // 传入正确的桶名，对象名和有效CannedACL，设置对象的ACL成功
    it("tc_set_object_acl_003", 0, async () => {
      const aclType = ['private', 'public-read', 'public-read-write']
      for (let acl of Object.values(aclType)) {
        const setObjectAclOutput = await getObsClient().setObjectAcl({
          Bucket: SIMPLE_BUCKET_NAME,
          Key: SIMPLE_OBJECT_KEY,
          Acl: acl as AclType
        });
        expect(setObjectAclOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      }
    });
    // 传入正确的桶名，对象名和无效CannedACL，设置对象的ACL失败
    it("tc_set_object_acl_004", 0, async () => {
      const setObjectAclOutput = await getObsClient().setObjectAcl({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        Acl: AclType.PUBLIC_READ_WRITE_DELIVERED
      });
      expect(setObjectAclOutput.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
      expect(setObjectAclOutput.CommonMsg.Code).assertEqual(ErrCode.InvalidArgument);
    });
    // 传入不存在的桶名，设置对象的ACL失败
    it("tc_set_object_acl_005", 0, async () => {
      const setObjectAclOutput = await getObsClient().setObjectAcl({
        Bucket: NO_EXIST_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        Acl: AclType.PRIVATE
      });
      expect(setObjectAclOutput.CommonMsg.Status).assertEqual(SERVER_ERROR_CODE);
      // expect(setObjectAclOutput.CommonMsg.Code).assertEqual(ErrCode.NoSuchBucket);
    });
    // 传入正确的桶名、有效的CannedACL和不存在的对象名，设置对象的ACL失败
    it("tc_set_object_acl_006", 0, async () => {
      const setObjectAclOutput = await getObsClient().setObjectAcl({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: NO_EXIST_OBJECT_KEY,
        Acl: AclType.PRIVATE
      });
      expect(setObjectAclOutput.CommonMsg.Status).assertEqual(SERVER_ERROR_CODE);
      expect(setObjectAclOutput.CommonMsg.Code).assertEqual(ErrCode.NoSuchKey);
    });
    // 传入正确的桶名，对象名和无效的ACL配置（OwnerId无效），设置对象的ACL失败
    it("tc_set_object_acl_007", 0, async () => {
      for (let Permission of Object.values(PermissionType)) {
        const setObjectAclOutput = await getObsClient().setObjectAcl({
          Bucket: SIMPLE_BUCKET_NAME,
          Key: SIMPLE_OBJECT_KEY,
          Owner: {
            ID: "err_owner_id"
          },
          Grants: [
            {
              Grantee: {
                Type: GranteeType.CANONICAL_USER, ID: USER_ID
              }, Permission
            }
          ]
        });
        expect(setObjectAclOutput.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
        expect(setObjectAclOutput.CommonMsg.Code).assertEqual(ErrCode.InvalidArgument);
      }
    });
    // 传入正确的桶名、对象名、有效的Owner信息、空的授权信息列表，设置对象的ACL失败
    it("tc_set_object_acl_008", 0, async () => {
      const setObjectAclOutput = await getObsClient().setObjectAcl({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: NO_EXIST_OBJECT_KEY,
        Owner: {
          ID: OWNER_ID
        },
        Grants: []
      });
      expect(setObjectAclOutput.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
      expect(setObjectAclOutput.CommonMsg.Code).assertEqual(ErrCode.MalformedACLError);
    });
    // 【PHP】【Go】传入正确的桶名、对象名、有效的Owner信息、无效的授权信息列表（用户ID无效），设置对象的ACL失败
    it("tc_set_object_acl_009", 0, async () => {
      const setObjectAclOutput = await getObsClient().setObjectAcl({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        Owner: {
          ID: OWNER_ID
        },
        Grants: [
          {
            Grantee: {
              Type: GranteeType.CANONICAL_USER, ID: "err_owner_id"
            }, Permission: PermissionType.READ
          }
        ]
      });
      expect(setObjectAclOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
    });
    // 传入正确的桶名、对象名、有效的Owner信息、无效的授权信息列表（用户ID无效），设置对象的ACL失败
    it("tc_set_object_acl_010", 0, async () => {
      const permissionType = ["NOSENCE"]
      for (let permission of Object.values(permissionType)) {
        const setObjectAclOutput = await getObsClient().setObjectAcl({
          Bucket: SIMPLE_BUCKET_NAME,
          Key: SIMPLE_OBJECT_KEY,
          Owner: {
            ID: OWNER_ID
          },
          Grants: [
            {
              Grantee: {
                Type: GranteeType.CANONICAL_USER, ID: "err_user_id"
              }, Permission: permission as PermissionType
            },
            {
              Grantee: {
                Type: GranteeType.GROUP, URI: "err_user_id"
              }, Permission: permission as PermissionType
            }
          ]
        });
        expect(setObjectAclOutput.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
        expect(setObjectAclOutput.CommonMsg.Code).assertEqual(ErrCode.MalformedACLError);
      }
    });
    // 传入正确的桶名、对象名、有效的Owner信息、无效的授权信息列表（用户ID无效），设置对象的ACL失败
    it("tc_set_object_acl_011", 0, async () => {
      const permissionType = ['EXCUET', 'ONLYREAD']
      for (let permission of Object.values(permissionType)) {
        const setObjectAclOutput = await getObsClient().setObjectAcl({
          Bucket: SIMPLE_BUCKET_NAME,
          Key: SIMPLE_OBJECT_KEY,
          Owner: {
            ID: OWNER_ID
          },
          Grants: [
            {
              Grantee: {
                Type: GranteeType.CANONICAL_USER, ID: "err_user_id"
              }, Permission: permission as PermissionType
            },
            {
              Grantee: {
                Type: GranteeType.GROUP, URI: "Everyone"
              }, Permission: permission as PermissionType
            }
          ]
        });
        expect(setObjectAclOutput.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
        expect(setObjectAclOutput.CommonMsg.Code).assertEqual(ErrCode.MalformedACLError);
      }
    });
    // TODO
    // 传入正确的桶名，对象名、有效的ACL配置和无效的versionId，设置指定版本对象ACL失败
    it("tc_set_object_acl_012", 0, async () => {
      const setObjectAclOutput = await getObsClient().setObjectAcl({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        VersionId: "no-version-id",
        Acl: AclType.PRIVATE
      });
      expect(setObjectAclOutput.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
      expect(setObjectAclOutput.CommonMsg.Code).assertEqual(ErrCode.InvalidArgument);
    });
  })
}