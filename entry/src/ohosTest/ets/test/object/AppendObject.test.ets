import { describe, it, expect, afterAll, beforeAll } from '@ohos/hypium';
import {
  assertIsCreateBucketSuccess,
  assertIsDeleteBucketSuccess,
} from '../common/helper';
import {
  ErrCode,
  EXPECT_CODE,
  NO_EXIST_BUCKET_NAME,
  FAILED_ERROR_CODE,
  REQUEST_ERROR_CODE,
  SERVER_ERROR_CODE,
  SIMPLE_OBJECT_KEY,
  SPECIAL_OBJECT_KEY,
} from '../common/const';
import { getObsClient } from '../common/testUtil';
import { AclType, StorageClassType } from '@obs/esdk-obs-harmony';

const SIMPLE_BUCKET_NAME = 'obs-sdk-test-bucket-for-append-object';
const object_data = 'abcdefghijklmnopqrstuvwxyz';

export default function appendObject() {
  describe("test_append_object", () => {
    beforeAll(async () => {
      await assertIsCreateBucketSuccess(SIMPLE_BUCKET_NAME);
    });
    afterAll(async () => {
      await assertIsDeleteBucketSuccess(SIMPLE_BUCKET_NAME)
    });
    // 传入正确的桶名和对象名，对象内容，追加上传对象成功；测试临时URL追加上传对象
    it("tc_append_object_001", 0, async () => {
      const key = `${SIMPLE_OBJECT_KEY}_tc_append_object_001`;
      // 追加写文件
      const appendObjectOutput = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
        Position: 0
      });
      expect(appendObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      expect(appendObjectOutput.InterfaceResult.NextPosition).assertEqual(26);
      expect(!!appendObjectOutput.InterfaceResult.ETag).assertTrue();

      // 下载对象
      const getObjectRes = await getObsClient().getObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
      });
      expect(getObjectRes.CommonMsg.Status).assertEqual(EXPECT_CODE);
      expect(getObjectRes.InterfaceResult.Content).assertEqual(object_data);
    });
    // 传入正确的桶名，对象名（对象名包含中文和特殊字符），对象内容，追加上传对象成功;测试临时URL追加上传对象
    it("tc_append_object_002", 0, async () => {
      const key = `${SPECIAL_OBJECT_KEY}_tc_append_object_002`;

      // 追加写文件
      const appendObjectOutput = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
        Position: 0
      });
      expect(appendObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      expect(appendObjectOutput.InterfaceResult.NextPosition).assertEqual(26);
      expect(!!appendObjectOutput.InterfaceResult.ETag).assertTrue();

      // 下载对象
      const getObjectRes = await getObsClient().getObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
      });
      expect(getObjectRes.CommonMsg.Status).assertEqual(EXPECT_CODE);
      expect(getObjectRes.InterfaceResult.Content).assertEqual(object_data);
    });
    // 传入正确的桶名，对象名，对象内容，无效的追加写位置（位置不为0），追加上传对象失败；测试临时URL追加上传对象
    it("tc_append_object_003", 0, async () => {
      const key = `${SIMPLE_OBJECT_KEY}_tc_append_object_003`;
      // 追加写文件
      const appendObjectOutput = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
        Position: 1
      });
      expect(appendObjectOutput.CommonMsg.Status).assertEqual(SERVER_ERROR_CODE);
      expect(appendObjectOutput.CommonMsg.Code).assertEqual(ErrCode.NoSuchKey);
    });
    // 传入不存在的桶名，追加上传对象失败；测试临时URL追加上传对象
    it("tc_append_object_004", 0, async () => {
      // 追加写文件
      const appendObjectOutput = await getObsClient().appendObject({
        Bucket: NO_EXIST_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        Body: object_data,
        Position: 0
      });
      expect(appendObjectOutput.CommonMsg.Status).assertEqual(SERVER_ERROR_CODE);
      // expect(appendObjectOutput.CommonMsg.Code).assertEqual(ErrCode.NoSuchBucket);
    });
    // 传入正确的桶名，对象名，对象内容和无效Content-MD5（与内容的MD5不匹配），追加上传对象失败；测试临时URL追加上传对象
    it("tc_append_object_005", 0, async () => {
      const key = `${SIMPLE_OBJECT_KEY}_tc_append_object_005`;
      // 追加写文件
      const appendObjectOutput = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
        Position: 0,
        ContentMD5: "no-content-md5"
      });
      expect(appendObjectOutput.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
      expect(appendObjectOutput.CommonMsg.Code).assertEqual(ErrCode.BadDigest);
    });
    // 传入正确的桶名、对象名，对象内容和不合法的预定义访问，追加上传对象失败；测试临时URL追加上传对象
    it("tc_append_object_006", 0, async () => {
      const key = `${SIMPLE_OBJECT_KEY}_tc_append_object_006`;
      // 传入错误的ACL，sdk内部会忽视掉该参数，预期是成功
      // 追加写文件
      const appendObjectOutput = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
        Position: 0,
        Acl: "private_read" as AclType
      });
      expect(appendObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      // expect(appendObjectOutput.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
      // expect(appendObjectOutput.CommonMsg.Code).assertEqual(ErrCode.InvalidArgument);
    });
    // 传入正确的桶名，对象名，对象内容和无效的StorageClass，追加上传对象失败；测试临时URL追加上传对象
    it("tc_append_object_007", 0, async () => {
      const key = `${SIMPLE_OBJECT_KEY}_tc_append_object_007`;
      // 传入错误的StorageClass，sdk内部会忽视掉该参数，预期是成功
      // 追加写文件
      const appendObjectOutput = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
        Position: 0,
        StorageClass: "standard_ai" as StorageClassType
      });
      expect(appendObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      // expect(appendObjectOutput.CommonMsg.Status).assertEqual(REQUEST_ERROR_CODE);
      // expect(appendObjectOutput.CommonMsg.Code).assertEqual(ErrCode.InvalidArgument);
    });
    // 传入正确的桶名，对象名，空的对象内容，追加上传对象成功；测试临时URL追加上传对象
    it("tc_append_object_008", 0, async () => {
      const key = `${SIMPLE_OBJECT_KEY}_tc_append_object_008`;
      // 追加写文件
      const appendObjectOutput = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: "",
        Position: 0,
      });
      expect(appendObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
    });
    // 传入正确的桶名，对象名，对象内容和包含中文的自定义元数据，追加上传对象成功；测试临时URL追加上传对象
    it("tc_append_object_009", 0, async () => {
      const key = `${SIMPLE_OBJECT_KEY}_tc_append_object_009`;
      // 追加写文件
      const appendObjectOutput = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
        Position: 0,
        Metadata: {
          "meta-test": "meta测试12"
        }
      });
      expect(appendObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      // 下载对象验证
      const getObjectRes = await getObsClient().getObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
      });
      expect(getObjectRes.CommonMsg.Status).assertEqual(EXPECT_CODE);
      expect(getObjectRes.InterfaceResult.Content).assertEqual(object_data);
      expect(getObjectRes.InterfaceResult.Metadata["meta-test"])
        .assertEqual(encodeURIComponent("meta测试12"));
    });
    // 传入正确的桶名，对象名，对象内容和包含中文的自定义元数据，追加上传对象成功；测试临时URL追加上传对象
    it("tc_append_object_010", 0, async () => {
      const key = `${SIMPLE_OBJECT_KEY}_tc_append_object_010`;
      // 追加写文件
      const appendObjectOutput = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
        Position: 0
      });
      expect(appendObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      // 第二次追加写
      const appendObjectOutput2 = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
        Position: object_data.length,
      });
      expect(appendObjectOutput2.CommonMsg.Status).assertEqual(EXPECT_CODE);
      expect(appendObjectOutput2.InterfaceResult.NextPosition).assertEqual(object_data.length * 2);
    });
    // 传入正确的桶名，对象名，内容，无效的追加写位置（位置不为上次追加写返回的值），追加上传对象失败；测试临时URL追加上传对象
    it("tc_append_object_011", 0, async () => {
      const key = `${SIMPLE_OBJECT_KEY}_tc_append_object_011`;
      // 追加写文件
      const appendObjectOutput = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
        Position: 0
      });
      expect(appendObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      // 第二次追加写
      const appendObjectOutput2 = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
        Position: object_data.length - 1,
      });
      expect(appendObjectOutput2.CommonMsg.Status).assertEqual(FAILED_ERROR_CODE);
      expect(appendObjectOutput2.CommonMsg.Code).assertEqual(ErrCode.PositionNotEqualToLength);
    });
    // 传入正确的桶名，已存在的对象名，内容，追加上传对象失败；测试临时URL追加上传对象
    it("tc_append_object_012", 0, async () => {
      const key = `${SIMPLE_OBJECT_KEY}_tc_append_object_012`;
      // 上传对象
      const putObjectOutput = await getObsClient().putObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
      });
      expect(putObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      // 第二次追加写
      const appendObjectOutput = await getObsClient().appendObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: key,
        Body: object_data,
        Position: object_data.length,
      });
      expect(appendObjectOutput.CommonMsg.Status).assertEqual(FAILED_ERROR_CODE);
      expect(appendObjectOutput.CommonMsg.Code).assertEqual(ErrCode.ObjectNotAppendable);
    });
  })
}