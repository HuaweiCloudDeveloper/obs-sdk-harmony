import { describe, it, expect, afterAll, beforeAll } from '@ohos/hypium';
import {
  assertIsCreateBucketSuccess,
  assertIsDeleteBucketSuccess,
  assertIsUploadObjectSuccess
} from '../common/helper';
import {
  ErrCode,
  EXPECT_CODE,
  NO_CONTENT_CODE,
  NO_EXIST_BUCKET_OBJECT_INPUT,
  NO_EXIST_OBJECT_KEY,
  SERVER_ERROR_CODE,
  SIMPLE_OBJECT_KEY
} from '../common/const';
import { getObsClient } from '../common/testUtil';

const SIMPLE_BUCKET_NAME = 'obs-sdk-test-bucket-for-delete-object-tagging';

export default function deleteObjectTagging() {
  describe("test_delete_object_tagging", () => {
    beforeAll(async () => {
      await assertIsCreateBucketSuccess(SIMPLE_BUCKET_NAME)
    });
    afterAll(async () => {
      await assertIsDeleteBucketSuccess(SIMPLE_BUCKET_NAME)
    });
    // 传入正确的桶名、对象名（不存在标签），删除对象标签成功
    it("tc_delete_object_tagging_001", 0, async () => {
      // 上传对象
      const putObjectRes = await getObsClient().putObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        Body: "Hello OBS!",
      });
      expect(putObjectRes.CommonMsg.Status).assertEqual(EXPECT_CODE);
      await assertIsUploadObjectSuccess(SIMPLE_BUCKET_NAME, SIMPLE_OBJECT_KEY);
      // 删除标签
      const deleteObjectOutput = await getObsClient().deleteObjectTagging({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY
      });
      expect(deleteObjectOutput.CommonMsg.Status).assertEqual(NO_CONTENT_CODE);
    });
    // 传入不存在的桶名、对象名，删除对象标签失败
    it("tc_delete_object_tagging_002", 0, async () => {
      const deleteObjectOutput = await getObsClient().deleteObjectTagging(NO_EXIST_BUCKET_OBJECT_INPUT);
      expect(deleteObjectOutput.CommonMsg.Status).assertEqual(SERVER_ERROR_CODE);
      // expect(deleteObjectOutput.CommonMsg.Code).assertEqual(ErrCode.NoSuchBucket);
    });
    // 传入桶名、不存在的对象名，删除对象标签失败
    it("tc_delete_object_tagging_003", 0, async () => {
      const deleteObjectOutput = await getObsClient().deleteObjectTagging({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: NO_EXIST_OBJECT_KEY
      });
      expect(deleteObjectOutput.CommonMsg.Status).assertEqual(SERVER_ERROR_CODE);
      expect(deleteObjectOutput.CommonMsg.Code).assertEqual(ErrCode.NoSuchKey);
    });
  })
}