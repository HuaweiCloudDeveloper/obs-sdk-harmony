import { describe, it, expect, afterAll, beforeAll } from '@ohos/hypium';
import {
  assertIsCreateBucketSuccess,
  assertIsDeleteBucketSuccess,
  assertIsUploadObjectSuccess,
} from '../common/helper';
import {
  ErrCode,
  EXPECT_CODE,
  NO_EXIST_BUCKET_NAME,
  NO_ALLOWED_CODE,
  SERVER_ERROR_CODE,
  SIMPLE_OBJECT_KEY,
  SIMPLE_OBJECT_KEY2,
  NO_EXIST_OBJECT_KEY,
  NO_CONTENT_CODE
} from '../common/const';
import { getObsClient } from '../common/testUtil';

const SIMPLE_BUCKET_NAME = 'obs-sdk-test-bucket-for-rename-object';
const POSIX_BUCKET_NAME = 'obs-sdk-test-bucket-for-rename-object-posix';
const originData = "abcdefghijklmnopqrstuvwxyz";

export default function renameObject() {
  describe("test_rename_object", () => {
    beforeAll(async () => {
      await assertIsCreateBucketSuccess(SIMPLE_BUCKET_NAME);
      await assertIsCreateBucketSuccess(POSIX_BUCKET_NAME, true);
    });
    afterAll(async () => {
      await assertIsDeleteBucketSuccess(SIMPLE_BUCKET_NAME);
      await assertIsDeleteBucketSuccess(POSIX_BUCKET_NAME);
    });
    // 传入对象桶，重命名对象失败；
    it("tc_obs_renameFile_001", 0, async () => {
      // 上传文件
      const putObjectOutput = await getObsClient().putObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        Body: originData,
      });
      expect(putObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      // 重命名对象
      const renameObjectOutput = await getObsClient().renameObject({
        Bucket: SIMPLE_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        NewObjectKey: SIMPLE_OBJECT_KEY2
      });
      expect(renameObjectOutput.CommonMsg.Status).assertEqual(NO_ALLOWED_CODE);
      expect(renameObjectOutput.CommonMsg.Code).assertEqual(ErrCode.MethodNotAllowed);
    });
    // 传入正确的并行文件系统，不存在的对象名，重命名对象失败；
    it("tc_obs_renameFile_002", 0, async () => {
      // 重命名对象
      const renameObjectOutput = await getObsClient().renameObject({
        Bucket: POSIX_BUCKET_NAME,
        Key: NO_EXIST_OBJECT_KEY,
        NewObjectKey: SIMPLE_OBJECT_KEY2
      });
      expect(renameObjectOutput.CommonMsg.Status).assertEqual(SERVER_ERROR_CODE);
      expect(renameObjectOutput.CommonMsg.Code).assertEqual(ErrCode.NoSuchKey);
    });
    // 传入不存在的桶，重命名对象失败
    it("tc_obs_renameFile_003", 0, async () => {
      // 重命名对象
      const renameObjectOutput = await getObsClient().renameObject({
        Bucket: NO_EXIST_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        NewObjectKey: SIMPLE_OBJECT_KEY2
      });
      expect(renameObjectOutput.CommonMsg.Status).assertEqual(SERVER_ERROR_CODE);
      // expect(renameObjectOutput.CommonMsg.Code).assertEqual(ErrCode.NoSuchBucket);
    });
    // 传入正确的桶，存在的对象名，重命名对象成功
    it("tc_obs_renameFile_004", 0, async () => {
      // 上传文件
      const putObjectOutput = await getObsClient().putObject({
        Bucket: POSIX_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        Body: originData,
      });
      expect(putObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      assertIsUploadObjectSuccess(POSIX_BUCKET_NAME, SIMPLE_OBJECT_KEY);
      // 重命名对象
      const renameObjectOutput = await getObsClient().renameObject({
        Bucket: POSIX_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        NewObjectKey: SIMPLE_OBJECT_KEY2
      });
      expect(renameObjectOutput.CommonMsg.Status).assertEqual(NO_CONTENT_CODE);
      assertIsUploadObjectSuccess(POSIX_BUCKET_NAME, SIMPLE_OBJECT_KEY2);
    });
    // 在文件桶调用接口renameFile并且objectKey存在和newObjectKey不存在
    it("tc_obs_renameFile_005", 0, async () => {
      // 上传文件
      const putObjectOutput = await getObsClient().putObject({
        Bucket: POSIX_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        Body: originData,
      });
      expect(putObjectOutput.CommonMsg.Status).assertEqual(EXPECT_CODE);
      assertIsUploadObjectSuccess(POSIX_BUCKET_NAME, SIMPLE_OBJECT_KEY);
      // 重命名对象
      const renameObjectOutput = await getObsClient().renameObject({
        Bucket: POSIX_BUCKET_NAME,
        Key: SIMPLE_OBJECT_KEY,
        NewObjectKey: SIMPLE_OBJECT_KEY2
      });
      expect(renameObjectOutput.CommonMsg.Status).assertEqual(NO_CONTENT_CODE);
      assertIsUploadObjectSuccess(POSIX_BUCKET_NAME, SIMPLE_OBJECT_KEY2);
    });
  })
}